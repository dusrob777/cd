<?PHP
	ob_start();
 	require_once("models/config.php");

	if(!isUserLoggedIn()) { header("Location: login.php"); die(); }
	header("Expires: Mon, 26 Jul 2010 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
	header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Content-Type: text/html; charset=utf-8");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0
	$submodule = (!empty($_GET['submodule']) && isset($_GET['submodule'])) ? $_GET['submodule'] : "";
	if (isset($_GET['r']) && !empty($_GET['r'])){
		if ($_GET['r'] == 'all'){
			$mysqli->query("UPDATE notifications_items set displayed=1 WHERE sent_to=".$loggedInUser->user_id);
		} else {
			$mysqli->query("UPDATE notifications_items set displayed=1 WHERE notify_id = ".$_GET['r']." AND sent_to=".$loggedInUser->user_id);
		}
	}

?>
<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title><?PHP echo $websiteName?></title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href="css/asset.css" rel="stylesheet" type="text/css" >
<link href="css/menu.css" rel="stylesheet" type="text/css" >
<!-- Bootstrap core CSS 
<link href="css/bootstrap.css" rel="stylesheet">-->
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
 <link rel="stylesheet" href="fonts/font-awesome4.1.0.css">
<link href="css/nanoscroller.css" rel="stylesheet" />
<link href="css/datetimepicker.css" rel="stylesheet" />
<link href="css/summernote.css" rel="stylesheet" />
<link href="css/summernote-bs3.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/icheckskins/flat/blue.css" rel="stylesheet">
<link href="css/helpdesk.css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> 


<script>

    $(document).ready(function() {
     

	    setTimeout(function() 
        { 
            $('#notifybar').slideUp(); 
        }, 5000); 
    });
	
	
</script>
<link rel = "stylesheet" type ="text/css" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css"/>


<!-- links to scroller in knowledgebase-->
<link rel="stylesheet" href="js/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/knowledgebase_scroller.css">

</head>
<body class="animated">
<div id="cl-wrapper">
  <div class="container-fluid" id="pcont"> 
    <!-- TOP NAVBAR -->
    <div id="head-nav" class="topbar navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="fa fa-bars"></span> </button>
        </div>
        <div class="navbar-collapse collapse">
          <div class="sidebar-logo">
            <div class="logo"> <a href="index.php?module=codedirectory"></a> </div>
          </div>
          <ul class="nav navbar-nav horizontal">    
           
            <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="index.php?module=codedirectory&submodule=home"><i class="fa fa-database" aria-hidden="true"></i> Code Directory</a></li>
             <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="index.php?module=codedirectory&submodule=calender"><i class="fa fa-calender" aria-hidden="true"></i> Calender</a></li>
              <li <?PHP echo ($_GET['module'] == 'fedex') ? ' class="active"' : ''?>><a href="index.php?module=fedex&submodule=test"><i class="fa fa-calender" aria-hidden="true"></i> Test</a></li>
          
          <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="https://calendar.google.com/calendar/embed?src=idylliccrm1%40gmail.com&ctz=America/Chicago"><i class="fa fa-calender" aria-hidden="true"></i> Calender</a></li>
      <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="index.php?module=fedex&submodule=main"><i class="fa fa-calender" aria-hidden="true"></i> FedEx</a></li>
       <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="index.php?module=gcalander&submodule=start"><i class="fa fa-calender" aria-hidden="true"></i> quickstart</a></li>
         <li <?PHP echo ($_GET['module'] == 'codedirectory') ? ' class="active"' : ''?>><a href="index.php?module=gcalander&submodule=oauth2callback"><i class="fa fa-calender" aria-hidden="true"></i> index</a></li>
                              
          </ul>
          <ul class="nav navbar-nav navbar-right user-nav" style="padding:5px">
            <li class="dropdown profile_menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;&nbsp; <span><?PHP echo $loggedInUser->displayname?></span> <b class="caret"></b></a>
              <ul class="dropdown-menu" style="margin:5px">
                <li><a href="#">My Account</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Messages</a></li>
                <li class="divider"></li>
                <li><a href="logout.php">Sign Out</a></li>
              </ul>
            </li>
          </ul>
           <ul class="nav navbar-nav not-nav navbar-right">
<?PHP
$data="SELECT n.notify_id,n.action,n.created,n.issueid,n.user_id,n.text FROM notifications n 
	INNER JOIN notifications_items ni 
	ON n.notify_id = ni.notify_id 
	WHERE ni.sent_to = ".$loggedInUser->user_id." and ni.displayed=0 ORDER BY n.created DESC";
$result = $mysqli->query($data);
$count = $result->num_rows;
if($count > 0){
?>          
         
            <li class="button dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class=" fa  fa-bell-o"></i><span class="bubble"><?PHP echo $count?></span></a>
              <ul class="dropdown-menu messages">
                <li>
                  <div class="nano nscroller">
                    <div class="content">
                      <ul>
<?PHP

while($row= $result->fetch_array()){
	if ($row['action'] == 'info'){
		$fa = 'fa-info-circle azure';	
	} else if ($row['action'] == 'warning'){
		$fa = 'fa-warning orange';	
	} else if ($row['action'] == 'alert'){
		$fa = ' fa-bell-o red';	
	} else {
		$fa = 'fa-question-circle';	
	}
?>                      
     <li> <a href="index.php?module=helpdesk&submodule=helpdesk_replies&id=<?PHP echo $row['issueid']?>&r=<?PHP echo $row['notify_id']?>"> <i class="fa <?PHP echo $fa?>"></i> <span class="date pull-right"><?PHP echo ConvertDateFormats('d M', $row['created'])?></span> <span class="name"><?PHP echo GetNamesFromID('users','display_name','','id', $row['user_id'])?></span> <?PHP echo $row['text']?> </a> </li>
<?PHP }
?>                        
                        
                      </ul>
                    </div>
                  </div>
                  <ul class="foot">
                    <li><a href="index.php?module=helpdesk&r=all">View all messages </a></li>
                  </ul>
                </li>
              </ul>
            </li>
<?PHP }
?>               
           <!--  <li class="button dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span class="bubble">2</span></a>
              <ul class="dropdown-menu">
                <li>
                  <div class="nano nscroller">
                    <div class="content">
                      <ul>
                        <li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Daniel</b> is now following you <span class="date">2 minutes ago.</span></a></li>
                        <li><a href="#"><i class="fa fa-male success"></i> <b>Michael</b> commented on your link <span class="date">15 minutes ago.</span></a></li>
                        <li><a href="#"><i class="fa fa-bug warning"></i> <b>Mia</b> commented on post <span class="date">30 minutes ago.</span></a></li>
                        <li><a href="#"><i class="fa fa-credit-card danger"></i> <b>Andrew</b> sent you a request <span class="date">1 hour ago.</span></a></li>
                      </ul>
                    </div>
                  </div>
                  <ul class="foot">
                    <li><a href="#">View all activity </a></li>
                  </ul>
                </li>
              </ul>
            </li>
           <!--  <li class="button"><a class="toggle-menu menu-right push-body" href="javascript:;" class="speech-button"><i class="fa fa-comments"></i></a></li>-->
          </ul>
        </div>
        <!--/.nav-collapse animate-collapse --> 
      </div>
    </div>
    <div class="cl-mcont">
    
  