<?php
// Copyright 2009, FedEx Corporation. All rights reserved.

// Version 6.0.0
define("ROOT",realpath($_SERVER['DOCUMENT_ROOT'].'/codedirectory'));//have to change a path here 

{
	
require_once( ROOT . '\vendor\FedExWebServicesStandard_PHP\TrackService_v12_php\php\library\fedex-common.php5');

//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = ROOT . "/vendor/FedExWebServiceStandardWSDL/TrackService/TrackService_v12.wsdl";

ini_set("soap.wsdl_cache_enabled", "0");

$client = new SoapClient($path_to_wsdl, array(
    //'trace' => 1,// finish here if server has a signed certificate
    'stream_context' => stream_context_create(array(
        'ssl' => array(
            'trace' => 1,
			'verify_peer' => false,
            'verify_peer_name' => false, 
            'allow_self_signed' => true //can fiddle with this one. Use this one if the server does not have a certificate 
        )
    )))); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information 

$request['WebAuthenticationDetail'] = array(
	'ParentCredential' => array(
		'Key' => getProperty('parentkey'), 
		'Password' => getProperty('parentpassword')
	),
	'UserCredential' => array(
		'Key' => getProperty('key'), 
		'Password' => getProperty('password')
	)
);

$request['ClientDetail'] = array(
	'AccountNumber' => getProperty('shipaccount'), 
	'MeterNumber' => getProperty('meter')
);
$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');
$request['Version'] = array(
	'ServiceId' => 'trck', 
	'Major' => '12', 
	'Intermediate' => '0', 
	'Minor' => '0'
);



$tnum=$_GET['tracknum'];// gets tracking number from URL 
if( isset($tnum) && $tnum!=''){
$request['SelectionDetails'] = array(
	'PackageIdentifier' => array(
		'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
		'Value' => $tnum // Replace 'XXX' with a valid tracking identifier
	)
);
 $request['ProcessingOptions']='INCLUDE_DETAILED_SCANS';// this is important for  getting history and is not in defaul fedex page
}
try {
	if(setEndpoint('changeEndpoint')){
		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
	}
	
	$response = $client ->track($request);

    if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
		if($response->HighestSeverity != 'SUCCESS'){
			echo '<table border="1">';
			echo '<tr><th>Track Reply</th><th> </th></tr>';
			trackDetails($response->Notifications, '');
			echo '</table>';
		}else{
			
	    	if ($response->CompletedTrackDetails->HighestSeverity != 'SUCCESS'){
				echo '<table border="1">';
			    echo '<tr><th>Shipment Level Tracking Details</th><th> </th></tr>';
			    trackDetails($response->CompletedTrackDetails, '');
				echo '</table>';
			}else{
				
			  //  trackDetails($response->CompletedTrackDetails->TrackDetails, '');
				//printNotifications($response -> TransactionDetail);
				//printNotifications($response -> Version);
				
				?>

<div class="row">
  <div class="col-lg-4 col-sm-6"> 
    <!-- START widget-->
    <div class="panel widget bg-purple">
      <div class="row row-table">
        <div class="col-xs-8 pv-lg">
          <div class="text-uppercase">Sender's Info</div>
          <div class="h6 mt0">
            <?PHP 
				//printNotifications($response->CompletedTrackDetails->TrackDetails->OriginLocationAddress, '');
				printNotifications($response->CompletedTrackDetails->TrackDetails->DatesOrTimes[1], '');
				printNotifications($response->CompletedTrackDetails->TrackDetails->ShipperAddress, ''); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-6"> 
    <!-- START widget-->
    <div class="panel widget bg-purple">
      <div class="row row-table">
        <div class="col-xs-8 pv-lg">
          <div class="text-uppercase">Current Status</div>
          <div class="h6 mt0">
            <?PHP 
			
				printNotifications($response->CompletedTrackDetails->TrackDetails->StatusDetail, ''); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-sm-12"> 
    <!-- START widget-->
    <div class="panel widget bg-green">
      <div class="row row-table">
        <div class="col-xs-8 pv-lg">
          <div class="text-uppercase">Receiver's  Info</div>
          <div class="h6 mt0">
            <?PHP 
				printNotifications($response->CompletedTrackDetails->TrackDetails->DatesOrTimes[0], '');			
				printNotifications($response->CompletedTrackDetails->TrackDetails->DestinationAddress, ''); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-6 col-sm-12"> 
    <!-- START widget-->
    <div class="panel widget bg-green">
      <div class="row row-table">
        <div class="col-xs-8 pv-lg">
          <div class="text-uppercase">Travel History</div>
          <div class="h6 mt0">
            <table>
              <tbody>
                <th>
                 <tr> <td><strong>Time</strong></td><td><strong>Event</strong></td><td><strong>City</strong></td><td><strong>State</strong></td></tr>
                  </th>
                
                <?PHP 
 
$arr=travelHistory($response->CompletedTrackDetails->TrackDetails->Events, '');

	?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?PHP
//echo htmlspecialchars($client->__getLastRequest());
// printNotifications($response->CompletedTrackDetails->TrackDetails->PackageDimensions, '');
//echo  trackDetails($response->CompletedTrackDetails->TrackDetails->DestinationAddress, '');
			
			}
		}
      // printSuccess($client,$response);
		
    }else{
        printError($client, $response);
    } 
    
    writeToLog($client);    // Write to log file   
} catch (SoapFault $exception) {
    printFault($exception, $client);
}


//echoing from response


}  