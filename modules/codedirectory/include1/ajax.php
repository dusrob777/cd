 
 
 
<?PHP
require("../../../models/config.php");
/*
	AUTOCOMPLETE
*/
// ADD PARENT
	
	if(isset($_GET['action']) && $_GET['action'] == 'addparent')
	{
		// id=assetlog, id3=adminlog, which are defined in there respecitve pages	
	?>

<form role="form" id="formparent" name="formparent">
  <div class="form-group">
    <label for="title">New Catgory name</label>
    <input type="text" class="form-control" id="parent"  name="parent" placeholder="Enter here" onkeyup="checkname();">
    <input type="hidden" id="hidparent"  name="hidparent" value="">
    <span id="name_status"></span> </div>
</form>
<?PHP
	}

	// INSERT PARENT or main category
	if((isset($_POST['action']) && ($_POST['action'] == 'insertparent')))
	{
		
		$data = array(
					   'parent' => 0,
					   'cat_name' => $db->escape($_POST['parent']), 
					  );
		// $content="'".$content."'";
		//$data1= htmlentities($data);
		$row = $db->insert('snip_category',$data) ;
		echo $db->last_query();
		echo var_dump($data);
	}
	
	
	
	// CHECK PARENT (main category)
	
	if(isset($_GET['action']) && $_GET['action'] == 'checkparent')
	{
		$name=$_POST['title'];
		$where=array('parent'=>0,'cat_name'=>$name);
		$category= $db->select('cat_name','parent')->from('snip_category')->where($where)->fetch();
			// echo $db->last_query();
		$affected = $db->affected_rows ;
		if (($affected)> 0)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
		exit();	
	}
/*
SUB Category
*/
//CHECK SUBCATegory
if(isset($_POST['action']) && $_POST['action'] == 'checksubcat')
	{
		$name=$_POST['parent'];
		
		
		$where=array('parent'=>$_POST['catname'],'cat_name'=>$name);
		$category= $db->select('cat_name','parent')->from('snip_category')->where($where)->fetch();
			// echo $db->last_query();
		$affected = $db->affected_rows ;
		if (($affected)> 0)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
		exit();	
	}
//ADD SUBCAT
if(isset($_GET['action']) && $_GET['action'] == 'addsubcat')
 {
	// id=assetlog, id3=adminlog, which are defined in there respecitve pages	
?>
<form role="form" id="formsubcat" name="formsubcat" >
  <div class="form-group">
    <label for="sel1">Select Category to add Sub Caterory</label>
    <?PHP 
	    $category=$db->from("snip_category")->fetch();
        $column = array();
	    $column1 = array();
        $categoryList = fetchCategoryTree($category);
    ?>
    <select class="form-control" id="catname" name="catname">
      <?php  
       foreach($categoryList as $cl)
         {
	       foreach($category as $cat) 
	         {
		        $column[] = $cat[parent];
		        $new=checkParent($category,$cat['cat_id']);
				array_push($column1,$new);
				//echo var_dump($column);
             }
	    	if( (in_array($cl['id'], $column1) || (in_array($cl['id'], $column)) || ($cl['parent']=='0') )    )
		       {
				?>
      <option value="<?php echo $cl["id"] ?>"><?php echo $cl["name"]; ?></option>
      <?PHP } else { ?>
      <optgroup label=<?php echo $cl["name"]; ?>></optgroup>
      <?php }			                    
		   	}?>
    </select>
  </div>
  <div class="form-group">
    <label for="title">Category Name</label>
    <input type="text" class="form-control" id="parent"  name="parent" placeholder="Enter title" onkeyup="checksubname();">
    <input type="hidden" id="hidsubcat"  name="hidsubcat" value="">
    <span id="subcat_status"></span> </div>
</form>
<?PHP
}
?>
<?PHP
//INSERT SUB CATEGORY
if((isset($_POST['action']) && ($_POST['action'] == 'insertsubcat')))
{
	$data = array(
		'parent' => $db->escape($_POST['catname']),
		'cat_name' => $db->escape($_POST['parent']), 
	);
	// $content="'".$content."'";
	//$data1= htmlentities($data);
	$row = $db->insert('snip_category',$data) ;
	//echo $db->last_query();
	//echo var_dump($data);
}
	
/*
Main code or file
*/
// Check for confliciting file name 
if(isset($_POST['action']) && $_POST['action'] == 'checkmainfilet')
	{
		$name=$_POST['title'];
		
		
		$where=array('cat_id'=>$_POST['catname'],'title'=>$name);
		$category= $db->select('title','parent')->from('snip_contents')->where($where)->fetch();
			// echo $db->last_query();
		$affected = $db->affected_rows ;
		if (($affected)> 0)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
		exit();	
	}
	

	// main file form
if(isset($_GET['action']) && $_GET['action'] == 'addfilemain') 
{
	// id=assetlog, id3=adminlog, which are defined in there respecitve pages	
?>
<form role="form" id="formaddmain" name="formaddmain" >
  <div class="form-group">
    <label for="sel1">Select Category </label>
    <?PHP 
		    $category=$db->from("snip_category")->fetch();
            $column = array();
		    $column1 = array();
            $categoryList = fetchCategoryTree($category);
    ?>
    <select class="form-control" id="catname" name="catname">
      <?php  
			 foreach($categoryList as $cl)
			    {
			       foreach($category as $cat) 
    			     {
	                     $column[] = $cat[parent];
						 $new=checkParent($category,$cat['cat_id']);
						 array_push($column1,$new);
                     }
				   if ((in_array($cl['id'], $column)) || (in_array($cl['id'], $column1)) || ($cl['parent']=='0') )
				     {
				?>
      <optgroup label=<?php echo $cl["name"]; ?>></optgroup>
      <?PHP } else	{?>
      <option value="<?php echo $cl["id"] ?>"><?php echo $cl["name"]; ?></option>
      <?php }			                    
			}?>
    </select>
  </div>
  <div class="form-group">
    <label for="title">Title:</label>
    <br />
    <span id="mainfile_status"></span>
    <input type="text" class="form-control" id="title"  name="title" placeholder="Enter title" onkeyup="checkFileMain();">
    <input type="hidden" id="hidfile"  name="hidfile" value="">
  </div>
  <div class="form-group">
    <label for="code">Code</label>
    <textarea class="form-control" rows="25" id="description" name="description" />
    
  </div>
</form>
<?PHP
}

if((isset($_POST['action']) && ($_POST['action'] == 'insertfilemain')))
{ 
	$con= base64_encode(($_POST['description']));
	$data = array(  	
		'cat_id' => $db->escape($_POST['catname']),
		'title' => $db->escape($_POST['title']), 
        'content' => $db->escape($con), 
    );	
    // $content="'".$content."'";	
   //$data1= htmlentities($data);
   $row = $db->insert('snip_contents',$data) ;
   // echo $db->last_query();
  //echo var_dump($data);
  //testarray($data); testpost($_POST);
}



?>
<?PHP

if(isset($_GET['action']) && $_GET['action'] == 'viewtitle')  
{
 ?>
<table aria-describedby="example_info" id="datatable1" class="table-striped  table-hover dataTable" style="border-top: outset #000" >
  <thead>
    <tr> <strong>
      <th > <?PHP 
	        $category=$db->from("snip_category")->fetch();
			
	        $id=$_GET['id'];                  
            $categoryList = fetchfileStr($category,$id);
		    $categoryList=array_reverse($categoryList);
			 //  echo var_dump($categoryList);
            foreach ($categoryList as $cl)
			{
				echo $cl['name']. "/";	
				
			}
			if( count($categoryList)>=3){
		?>
        <button type="button" class="btnaddcode btn-primary btn-sm pull-right" id-add="<?PHP echo $_GET['id'];?>">New</button>
        <?PHP } ?>
      <td><a href = "index.php?module=codedirectory&submodule=home" class="btn btn-danger  btn-xs" onclick="deleterows(<?PHP echo $_GET['id'];?>,'modules/codedirectory/include1/ajax.php','delete_folder','row','')"><i class="fa fa-trash-o"></i></a></td>
       
      </th>
      </strong> </tr>
  </thead>
  <tbody aria-relevant="all" aria-live="polite" role="alert" style="overflow-y: hidden; ">
    <?PHP 
	
	 $rows=$db->from("`snip_contents`")->where("`cat_id`" , $_GET['id'])->order_by("con_id", "desc")->fetch();	
	
 if( $db->affected_rows == '0')
 {
	 ?>
    <tr >
      <td>No data to display</td>
      <td></td>
    </tr>
    <?PHP
 }
		   foreach ($rows as $row) {
				
			   ?>
    <tr id="row_<?PHP echo $row['con_id']?>" class="gotourl" data-id="<?PHP echo $row['con_id']?>" >
      <td><i class="fa fa-file-text"></i><?PHP echo $row['title'];?>  </td>
      <td> <a href = "javascript:void(0)" class="btn btn-danger  btn-xs" onclick="deleterows(<?PHP echo $row['con_id']?>,'modules/codedirectory/include1/ajax.php','delete_codes','row','')"><i class="fa fa-trash-o"></i></a></td>
    </tr>
    <?PHP
		 	  }
?>
  </tbody>
</table>
<?PHP
}

if(isset($_GET['action']) && $_GET['action'] == 'delete_codes') {
	//$del=$_GET['id'];
	
	$db->delete()->from('snip_contents')->where('con_id', $_GET['id'])->execute();
	echo $db->last_query();
}	

if(isset($_GET['action']) && $_GET['action'] == 'delete_folder') {
	//$del=$_GET['id'];
	
	$db->delete()->from('snip_category')->where('cat_id', $_GET['id'])->execute();
	//$db->delete()->from('snip_category')->where('parent', $_GET['id'])->execute();
	$db->delete()->from('snip_contents')->where('cat_id', $_GET['id'])->execute();
	 $category=$db->from("snip_category")->fetch();
	 $id=$_GET['id'];
	 $categoryList = childId($category,$id);
	 foreach($categoryList as $cl)
	 {
	$db->delete()->from('snip_category')->where('cat_id', $cl['id'])->execute();	
	$db->delete()->from('snip_contents')->where('cat_id', $cl['id'])->execute(); 
	 }
}	



if((isset($_POST['action']) && ($_POST['action'] == 'insertfile')))
{
 	$con= base64_encode(($_POST['description']));
	$data = array(
    				'cat_id' =>$_POST['hidid'],
    				'title' => $_POST['title'],
					 'content' => $db->escape($con),
    				//'content' => $_POST['editor1']
				);
	//$data1= htmlentities($data);
	$row = $db->insert('snip_contents',$data) ;
	//echo $db->last_query();
	//echo var_dump($data);
}

     
if(isset($_GET['action']) && $_GET['action'] == 'addfile')
{
	// id=assetlog, id3=adminlog, which are defined in there respecitve pages	
?>
<form role="form" id="formadd" name="formadd">
  <input name="hidid" type="hidden" value="<?PHP echo $_GET['id1']; ?>" id="hidid" />
  <div class="form-group">
    <label for="title">Title:</label>
     <span id="mainfile_status1"></span>
    <input type="text" class="form-control" id="title"  name="title" placeholder="Enter title" onkeyup="checkFileMain1();">
     <input type="hidden" id="hidfile1"  name="hidfile1" value="">
  </div>
  <div class="form-group">
    <label for="code">Code</label>
    <textarea class="form-control" rows="25" id="description" name="description" />
    <div id="description"/>
  </div>
</form>
<?PHP
}
if(isset($_POST['action']) && $_POST['action'] == 'checkmainfile1')//this is test from the titles section new button
	{
		$name=$_POST['title'];
		
		
		$where=array('cat_id'=>$_POST['hidid'],'title'=>$name);
		$category= $db->select('title','parent')->from('snip_contents')->where($where)->fetch();
			// echo $db->last_query();
		$affected = $db->affected_rows ;
		if (($affected)> 0)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
		exit();	
	}
?>
<?PHP
if(isset($_GET['action']) && $_GET['action'] == 'searchview')  
{
	if (!empty($_GET['id']))
	 {
		 $search=$_GET['id'];
        	$rows=$db->from("snip_contents")->like("title" , $search, 'both')->fetch();
     }else 
	 {
	$search=$_GET['id'];
        	$rows=$db->from("snip_contents")->like("title" , $search, 'none')->fetch();
	
	 }
  ?>
<table aria-describedby="example_info" id="datatable1" class="table-striped  table-hover dataTable" style="border-top: outset #000" >
<thead>
  <tr> <strong>
    <th > Search Results </th>
    </strong> </tr>
</thead>
<tbody aria-relevant="all" aria-live="polite" role="alert" style="overflow-y: hidden;">
  <?PHP
		     foreach ($rows as $row)
		      { 
    ?>
  <tr id="row_<?PHP echo $row['con_id']?>" class="gotourl"  data-id="<?PHP echo $row['con_id']?>">
    <td height="20"><i class="fa fa-file-text"></i> <?PHP echo "<strong>". $row['title']."</strong><br/>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp";
			 
			          $category1=$db->from("snip_category")->fetch();
				      $id1=$row['cat_id'];                  
                      $categoryList1= fetchfileStr($category1,$id1);
				      $categoryList1=array_reverse($categoryList1);
					  foreach ($categoryList1 as $cl1)
			     { 
				  echo "<em>". $cl1['name']. "/"."</em>";
		         }
				      //  echo var_dump($categoryList);			 
	            ?></td>
  </tr>
  <?PHP
		       }		   
}

?>
  <script>

$(document).ready(function()
{	//var id1; //id of a list item after list is  clicked on gotourl list 
	var val2;// content loaded in the editor when list is clicked ( old Value)
	var val;	
	var id3=$('.gotourl').attr("data-id");// id of list item when the list is loaded automatically on the first time
     //content inside the editor before clickinng save button  (new Value)
	//alert(first);// this loads the code editor ( content) with the 
	$.ajax
	({ //create an ajax request to load_page.php
		type: "GET",
		url: "modules/codedirectory/include1/contents.php",
		data:'id='+id3,
		success: function (response)			
		{  
			// $("#content").html(response);
			var editor = ace.edit("content");
			editor.setValue(response);
			val1= editor.setValue(response);
			editor.setBehavioursEnabled(true);
			editor.setTheme("ace/theme/tomorrow");
			editor.getSession().setMode("ace/mode/php");			 		            
			editor.getSession().on('change', function()
			{
            	val = editor.getSession().getValue();
            })
		    editor.commands.addCommand        	
			({
   				name: 'save',
                bindKey: {win: "Ctrl-S", "mac": "Cmd-S"},
				exec: function(editor)
				{
                	//console.log("saving", editor.session.getValue())
		            var content=editor.session.getValue();
					if (val1 === content)
					{
						alert (" Document has not changed");
						exit;
				    }
				    else( confirm("Do you want to save changes") == true)
					{
		            	$.ajax
		                ({ //create an ajax request to load_page.php
                        	type:"POST",
			                url: "modules/codedirectory/include1/save.php",
		                    data: {"id": id3 , "content":content  },
                            success: function (msg)
			                {
				            	 Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);				
			                }			
	                    });
			        } 
			    }
	 	  });
  	  }
   });
   });		 	 
/*$('.btnSave').on("click", function()
{
	 id4=$('.gotourl').attr("data-id");
	alert ("inside on select id4 = " +id4);
	alert ("test");
	alert(val);
	alert(val1);
	
	if (typeof id4 != 'undefined') //checks if this is the first load oe not if id1 is set then it is not the first load
	{
		var newvalue=  val;
		//newvalue= "'"+newvalue1+"'" ;
		//var newvalue= content;
		//alert("new"+newvalue);
	    var oldvalue= val2;
	    //alert("old" +oldvalue);
	   if (newvalue === oldvalue)
	   {
			alert (" Document has not changed");
			exit;
	   }
	   else( confirm("Do you want to save changes") == true)
	   {
		  	$.ajax
		    ({ //create an ajax request to load_page.php
           		type:"POST",
				url: "modules/codedirectory/include1/save.php",
		   	    data: {"id": id4 , "content":newvalue  },
           	 	success: function (msg)
				{
			   	    Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);				
				}			
	        });
		}
					  
	}
		
	 
//var id4= undefined;
});	*/
	
	
$(".btnaddcode").click(function() 
{
	var id1=$(this).attr("id-add");
    $("#myModal1 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addfile&id1=' + id1);
    // alert(id1);
   $("#myModal1").modal();
});

$('#datatable1 tbody tr').on('click', function(event)
{	
	$('#datatable1 tbody tr:first-child').addClass('plain');
    $(this).addClass('highlight').siblings().removeClass('highlight');	
});		
</script> 



  



  



