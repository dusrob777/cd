 
<?PHP 
?>
<style type="text/css" media="screen">
#mainfile_status, #mainfile_status1, #subcat_status, #name_status {
	color: #F00;
}
#datatable1.table-striped tbody tr.highlight td {
	background-color: #3399CC;
	color: #FFF;
}
#datatable1.table-striped tbody tr:first-child {
	background-color: #3399CC;
	color: #FFF;
}
#datatable1.table-striped tbody tr.plain td {
	background-color: #F5F5F5;
	color: #333;
}
div#layoutObj {
	position: relative;
	margin-top: 0px;
	margin-left: 0px;
	width: 1850px;
	height: 800px;
	overflow-y: auto;
	overflow-x: hidden;
}
#toptext {
	margin: 0;
	position: absolute;
	top: 0;
	bottom: 0;
	left: -2px;
	right: 0;
	overflow-y: scroll;
	overflow-x: hidden;
}
#content {
	margin: 0;
	position: absolute;
	top: 0;
	bottom: 0;
	left: -2px;
	right: 0;
	overflow-y: scroll;
	overflow-x: hidden;
}
</style>

<script>

function doOnLoad()
{	
  var myLayout, layout1,myContextMenu1;	  	
  myLayout = new dhtmlXLayoutObject
	({
		parent:"layoutObj", pattern: "3W", 
	});
  myLayout.cells("a").setWidth(300);
  //myLayout.cells("a").attachObject("objId");
  myLayout.cells("b").setHeight(200);
  myLayout.cells("b").setWidth(375);
  myLayout.cells("c").setHeight(600);			
  myLayout.cells("b").attachObject("toptext");
  myLayout.cells("c").attachObject("content");			
  myTreeView = myLayout.cells("a").attachTreeView
  	({
 		context_menu: true,	
		iconset: "font_awesome",		
	   	json: "modules/codedirectory/tree.php",
		
    });	
  myTreeView.enableContextMenu(true);
  myTreeView.attachEvent("onContextMenu", function(id,  zoneId, cas)
  	{
       return true; // to prevent default context menu
    });
  myContextMenu1 = new dhtmlXMenuObject
  	({
		parent: "content",
		icons_path: "vendors/menu/samples/dhtmlxMenu/common/imgs/",
		context: true
	});
  myContextMenu1.addNewChild(myContextMenu1.topId, 0, 0, "Add Category", false, "select_all.gif");
  myContextMenu1.addNewChild(myContextMenu1.topId, 1, 1, "Add  Sub Category", false, "save.gif");
  myContextMenu1.addNewChild(myContextMenu1.topId, 2, 2, "Add Code", false, "new.gif");			
  myContextMenu1.attachEvent("onClick", function(id, zoneId, cas)
   {
		//var casText = "";
		//for (var a in {ctrl:1,alt:1,shift:1}) casText += " "+a+"="+(cas[a]==true?"true":"false");
		if(id==0)
		  {    //printLog("<b>onClick</b> zone="+zoneId+", id="+id+", <br>");
			  $("#myModal4 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addparent' );
			  $("#myModal4").modal();
		  }
		else if(id==1)
		  {
			   $("#myModal5 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addsubcat' );
			   $("#myModal5").modal();
		  }
		else
	      {
	       	   $("#myModal2 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addfilemain' );
			   $("#myModal2").modal();
	      }
   });					
					
  myTreeView.attachEvent("onSelect", function(id)
	{
	    $('.btnSave').hide();
	    var id = myTreeView.getSelectedId();
		var id4;
		//var id1 = myTreeView.getItemText(id)
		//alert(id);
		if(id != '' && id != null)
		  {
		 	$("#toptext").load('modules/codedirectory/include1/ajax.php?action=viewtitle&id=' + id );
   		  }
		           // $("#toptext").show();
	});			
  myLayout.cells("b").setText("Titles ");
  myLayout.cells("c").setText('<button type="button"  class="btnSave btn-default btn-sm pull-right" style="display:none;" >Save</button>Code &nbsp;&nbsp;&nbsp;&nbsp;(press CRTL+S to save)');
  myLayout.cells("a").setText('<div id ="new"><button type="button" id="btnNew" class="btnNew btn-success btn-sm pull-right">New</button></div>Cateogories');
}
		

		
	</script>

<body onLoad="doOnLoad();">
<!--<div id="ta" style="width: 500px; height: 160px; border: #909090 1px solid; overflow: auto; font-size: 11px; font-family: Tahoma;"></div
 
  > <div class="col-sm-6">
 

</div>
 <div class="col-sm-6">
 <button type="button" class="btnaddtop btn-info pull-right"><span class="glyphicon glyphicon-plus"></span>Add file</button>

</div>
--><!--<form method="post" name="sform" id="sform" class="form-inline">    
  
      <div class="form-group ">
      <input class="form-control input-sm " placeholder="Type here to search"  id="inpsearch" name="inpsearch" type="text">
      <input type="button" class="btn btn-primary btn-sm pull-right" id="search" value="Search">   
    </form>
 </div>
 -->
<div class="input-group p_search">
  <input class="form-control" name="inpsearch" id="inpsearch" placeholder="Type your keyword for search...." type="text">
  <span class="input-group-btn">
  <input type="button" class="btn btn-danger btn-sm pull-right" id="search" value="Search">
  </span>
</div>

<!-- <div id="myDiv" style="display:none;">
<button id="parentBtn" class="btn btn-danger btn-xs">Main Category</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="subcatBtn"  class="btn btn-danger btn-xs">Sub-Category</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="codeBtn" class="btn btn-danger btn-xs"
 > Code</button>
</div>  
-->
<div class="card-block" id="myDiv" style="display:none;">
  <button type="button"  id="parentBtn" class="btn btn-success btn-sm">Main Category</button>
  <button type="button"  id="subcatBtn" class="btn btn-success btn-sm">Sub-Category</button>
  <button type="button"  id="codeBtn" class="btn btn-success btn-sm">Code</button>
</div>
<div id="layoutObj"></div>
<div id="toptext" > </div>
<div id="content" > </div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class=" icon-plus2"></i> Add file</h4>
      </div>
      <!-- /modal-header -->
      <div class="modal-body"> loading. please wait ... </div>
      <!-- /modal-body -->
      <div class="modal-footer"> <span id="loading"></span>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
        <button type="button" class="btn btn-primary" id="btnadd">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class=" icon-plus2"></i> Add file</h4>
      </div>
      <!-- /modal-header -->
      <div class="modal-body"> loading. please wait ... </div>
      <!-- /modal-body -->
      <div class="modal-footer"> <span id="loading"></span>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
        <button type="button" class="btn btn-primary" id="btnadd2">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class=" icon-plus2"></i> Add Category</h4>
      </div>
      <!-- /modal-header -->
      <div class="modal-body"> loading. please wait ... </div>
      <!-- /modal-body -->
      <div class="modal-footer"> <span id="loading"></span>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
        <button type="button" class="btn btn-primary" id="btnadd4">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class=" icon-plus2"></i> Add Sub-Category</h4>
      </div>
      <!-- /modal-header -->
      <div class="modal-body"> loading. please wait ... </div>
      <!-- /modal-body -->
      <div class="modal-footer"> <span id="loading"></span>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
        <button type="button" class="btn btn-primary" id="btnadd5">Save</button>
      </div>
    </div>
  </div>
</div>
</body>
<script>

$("#parentBtn").click(function(){
	 $("#myModal4 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addparent' );
	 $("#myModal4").modal();
	 $("#myDiv").hide();
});

$("#subcatBtn").click(function(){
	$("#myModal5 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addsubcat' );
	 $("#myModal5").modal();
	  $("#myDiv").hide();
});
$("#codeBtn").click(function(){
	 $("#myModal2 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addfilemain' );
				$("#myModal2").modal();
				 $("#myDiv").hide();
});


$(document).on("click", "#btnNew", function(){
	//e.preventDefault();
	$("#myDiv").toggle();
	//e.preventDefault();
});
$("#inpsearch").keypress(function(event) {
    if (event.which == 13) {
		
      //alert("You pressed enter");
      event.preventDefault();
     $('#search').click();
      //  validate(); doesn't need to be called from here
    }

});

	
$(".btnaddtop").click(function() 
	  {
		 //var id1=$(this).attr("id-add");
         $("#myModal2 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addfilemain');		
		 $("#myModal2").modal();
     });
	 
 function checkFileMain()
    {
	
	var getvalues = $("#formaddmain").serialize();
		var name=$( '#title' ).val();
		//var catname=$('#catname').val();
		//alert (name);
		if(name.length > 3)
		{
			$.ajax({
				type: 'post',
				url: 'modules/codedirectory/include1/ajax.php',
				data: 'action=checkmainfilet&'+getvalues ,
				success: function (response) {
					if(response == 1){
						$( '#mainfile_status' ).html('Title is not available');
						$("#hidfile").val('1');
					} else {
						$( '#mainfile_status' ).html('Title is available');
						$("#hidfile").val('0');
					}
				}
			   });
		}
		else
		{
			$( '#mainfile_status' ).html("Title should be more than 3 letters");
			$("#hidfile").val('1');
			
		}
	
	}	 	 

	 
 
$(document).on("click", "#btnadd2", function()
	{
		var getvalues = $("#formaddmain").serialize();
      
	  var check=$( '#hidfile' ).val()//var check=$('#name_status').val();
		//var check=document.getElementById("name_status").innerHTML;
	//	alert(check);
		if(check == 0)
		  {	$.ajax
		({
			type: "POST",
            url: "modules/codedirectory/include1/ajax.php",
            data: "action=insertfilemain&"+getvalues,
		    success: function(msg)
			{
				Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);
				var id2= $("#catname").val();
				//alert(id2);
			  $("#toptext").load('modules/codedirectory/include1/ajax.php?action=viewtitle&id=' + id2 );
			 // load('modules/codedirectory/include1/home.php?titleid=' + id2 );
				$("#myModal2").modal('hide');
    	    }	
        });
		  }
		 else 
		{		alert("Edit Title");
		//exit();
		} 
		
      });


function checkFileMain1()
    {
	
	var getvalues = $("#formadd").serialize();
		var name=$( '#title' ).val();
		//var catname=$('#catname').val();
		
		if(name.length > 3)
		{
			$.ajax({
				type: 'post',
				url: 'modules/codedirectory/include1/ajax.php',
				data: 'action=checkmainfile1&'+getvalues ,
				success: function (response) {
					if(response == 1){
						$( '#mainfile_status1' ).html('Title is not available');
						$("#hidfile1").val('1');
					} else {
						$( '#mainfile_status1' ).html('Title is available');
						$("#hidfile1").val('0');
					}
				}
			   });
		}
		else
		{
			$( '#mainfile_status1' ).html("Title should be more than 3 letters");
			$("#hidfile1").val('1');
			
		}
	
	}	 	 

 
$(document).on("click", "#btnadd", function()
	{
		var id1=$(this).attr("id-add");
		var getvalues = $("#formadd").serialize();
		var check=$( '#hidfile1' ).val()
    if(check == 0)
		  {	$.ajax
		({
			type: "POST",
            url: "modules/codedirectory/include1/ajax.php",
            data: "action=insertfile&"+getvalues,
		    success: function(msg)
			{
				Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);
				var rows = '<tr id="row_' + $("#hidid").val() + '">' +'<td>' + '<i class="fa fa-file-text"></i>' + $("#title") + '</td>' + '</tr>';
			  console.log(rows);
			  $("#datatable1 tbody").prependTo(rows);
			  var id2= $("#hidid").val();
			  $("#toptext").load('modules/codedirectory/include1/ajax.php?action=viewtitle&id=' + id2 );				
	   		  $("#myModal1").modal('hide'); 
			}
			
        });
		  }
		 else 
		{		alert("Edit Title");
		//exit();
		}   
		
    });
$(document).on("click", "#search", function()
	{
		
		 
				var id2= $("#inpsearch").val();
				//alert(id2);
			  $("#toptext").load('modules/codedirectory/include1/ajax.php?action=searchview&id=' + id2 );
				$("#myModal2").modal('hide');	
    	   	
       
		
      });
	  	  
	  
$(".btnaddcat").click(function() 
	  {
		 //var id1=$(this).attr("id-add");
         $("#myModal3 .modal-body").load('modules/codedirectory/include1/ajax.php?action=addcat');		
		 $("#myModal3").modal();
     });		
	 /*
	 	AUTOCOMPLETE for main category
	 */

	 
$(document).on("click", "#btnadd4", function()
	{	
 //var check=document.getElementById("name_status").innerHTML;


		var getvalues = $("#formparent").serialize();
		var check=$( '#hidparent' ).val()//var check=$('#name_status').val();
		//var check=document.getElementById("name_status").innerHTML;
	//	alert(check);
		if(check == 0)
		  {
				$.ajax
				({
					type: "POST",
					url: "modules/codedirectory/include1/ajax.php",
					data: "action=insertparent&"+getvalues,
					success: function(msg)
					{
						Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);
						$("#myModal4").modal('hide');
						window.location.reload();				
					  }			 
				 });
		  }
		else 
		{		alert("Edit Parent Name");
		//exit();
		}
    });
	

	  // main category autocomplete function check
    function checkname()
    {
	
		var name=$( '#parent' ).val();
		
		if(name.length > 3)
		{
			$.ajax({
				type: 'post',
				url: 'modules/codedirectory/include1/ajax.php?action=checkparent',
				data: {	title:name,	},
				success: function (response) {
					if(response == 1){
						$( '#name_status' ).html('Title is not available');
						$("#hidparent").val('1');
					} else {
						$( '#name_status' ).html('Title is available');
						$("#hidparent").val('0');
					}
				}
			   });
		}
		else
		{
			$( '#name_status' ).html("Title should be more than 3 letters");
			$("#hidparent").val('1');
			
		}
	
	}
	
	//autocheck function for subcategory	
	 function checksubname()
    {
	
	var getvalues = $("#formsubcat").serialize();
		var name=$( '#parent' ).val();
		//var catname=$('#catname').val();
		
		if(name.length > 3)
		{
			$.ajax({
				type: 'post',
				url: 'modules/codedirectory/include1/ajax.php',
				data: 'action=checksubcat&'+getvalues ,
				success: function (response) {
					if(response == 1){
						$( '#subcat_status' ).html('Title is not available');
						$("#hidsubcat").val('1');
					} else {
						$( '#subcat_status' ).html('Title is available');
						$("#hidsubcat").val('0');
					}
				}
			   });
		}
		else
		{
			$( '#subcat_status' ).html("Title should be more than 3 letters");
			$("#hidsubcat").val('1');
			
		}
	
	}
	
	// Add subcategory	
$(document).on("click", "#btnadd5", function()
	{
		var getvalues = $("#formsubcat").serialize();
	var check=$( '#hidsubcat' ).val()//var check=$('#name_status').val();
		//var check=document.getElementById("name_status").innerHTML;
	//	alert(check);
		if(check == 0)
		  {
				
	
		var getvalues = $("#formsubcat").serialize();
      	$.ajax
		({
			type: "POST",
            url: "modules/codedirectory/include1/ajax.php",
            data: "action=insertsubcat&"+getvalues,
		    success: function(msg)
			{
				Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);
				$("#myModal5").modal('hide');
				window.location.reload();				
	   		}				
		 });
		  }
		else 
		{		alert("edit Parent Name");
		//exit();
		}
    });
	
$(document).on("click",'.gotourl',function()
{
	$('.btnSave').show();
	id1=$(this).attr("data-id");
	id4=undefined;
	//alert("inside gotourl id4 = " +id4);
	//alert(first);// this loads the code editor ( content) with the 
	$.ajax
	({ //create an ajax request to load_page.php
		type: "GET",
		url: "modules/codedirectory/include1/contents.php",
		data:'id='+id1,
		success: function (response)			
		{  // $("#content").html(response);
			var id = $(this).attr("data-id");
			var editor = ace.edit("content");
		    editor.setValue(response);
		    val2= editor.setValue(response);
		    editor.setBehavioursEnabled(true);
		    editor.setTheme("ace/theme/tomorrow");
			editor.getSession().setMode("ace/mode/php"); 
			editor.commands.addCommand        	
			({
   				name: 'save',
                bindKey: {win: "Ctrl-S", "mac": "Cmd-S"},
				exec: function(editor)
				{
                	//console.log("saving", editor.session.getValue())
		            var content=editor.session.getValue();
					//alert(content);
					//alert(val2);		
					if (val2 === content)
					{
						alert (" Document has not changed");
						exit;
				    }
				    else( confirm("Do you want to save changes") == true)
					{
		            	$.ajax
		                ({ //create an ajax request to load_page.php
                        	type:"POST",
			                url: "modules/codedirectory/include1/save.php",
		                    data: {"id": id1 , "content":content  },
                            success: function (msg)
			                {
				            	 Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);				
			                }			
	                    });
					 } 
				}
			});
			editor.getSession().on('change', function()
			{
              	 val = editor.getSession().getValue();
            });
        }
	});	
	
	
});
$(document).on("click",'.btnSave',function()

{
	//alert (id1);
	if (typeof id1 != 'undefined') //checks if this is the first load oe not if id1 is set then it is not the first load
	{
		var newvalue=  val;
		//newvalue= "'"+newvalue1+"'" ;
		//var newvalue= content;
		//alert("new"+newvalue);
	    var oldvalue= val2;
	    //alert("old" +oldvalue);
	   if (newvalue === oldvalue)
	   {
			alert (" Document has not changed");
			exit;
	   }
	  // else( confirm("Do you want to save changes") == true)
	   {
		  	$.ajax
		    ({ //create an ajax request to load_page.php
           		type:"POST",
				url: "modules/codedirectory/include1/save.php",
		   	    data: {"id": id1 , "content":newvalue  },
           	 	success: function (msg)
				{
			   	    Notify('Record Successfully Updated', 'top-right', '5000', 'success', 'fa-check', true);				
				}			
	        });
		}
					  
	}
		
});	 
 		
		
	</script>
  



