<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p><strong>Outputting the final PDF<br />
</strong>When you&rsquo;ve finished creating all required cells, images, links, text etc. you have to call the <a title="TCPDF Output method" href="http://www.tecnick.com/pagefiles/tcpdf/doc/com-tecnick-tcpdf/TCPDF.html#methodOutput" target="_blank">Output()</a> method to actually get your hands on your dynamically created PDF. This can take no parameters in which case the PDF is sent to the browser, more commonly though, developers specify the filename and the destination of the generated PDF. The destination can be one of four values, these are:</p>
<p>I: send the file inline to the browser.<br />
  D: send to the browser and force a file download with the name given by name.<br />
  F: save to a local file with the name given by name.<br />
  S: return the document as a string.</p>
<p>You can see my code sets the destination value as F:</p>
<p><em>$pdf-&gt;Output(&rdquo;./pdfs/example.pdf&rdquo;, &ldquo;F&rdquo;);</em></p>
<p>this is telling TCPDF to save the dynamically generated PDF document in the pdfs folder with the name example.pdf. On Windows it&rsquo;s not needed but on Unix based machines you will need to set appropriate permissions on the pdfs (or whatever) folder to allow TCPDF to write the pdfs to it.</p>
<p>A little tip when your developing locally (as opposed to directly on your webhost) and using &lsquo;F&rsquo; for the destination parameter is to create your PDFs with a random filename so you can simply press refresh on your script that does the PDF creation logic. If you have a static filename as I do in this example (called example.pdf) and you have the last generated PDF file (also example.pdf) open TCPDF will not be able to write the PDF (as it is aleady open, so a sharing violation error will occur internally). What I often use for random filenames during development is <em>sha1(microtime()),</em> this means to check changes I just need to press refresh on my PHP script and then visit my PDFs folder without having to close previous versions of my PDF.</p>
<p>S is useful if you want to sent the PDF as an attachment in an email without first saving it to disk somewhere.</p>
<p>Both I and D allow you to access the PDF quickly via the browser. A note about these two lads is this… Internet Explorer often looks at the extension of the file, (which will be .php) and assumes that the output will be HTML and thus will not present you a PDF, it will likely present a load of binary data in the webpage itself which obviously is not what you want. Firefox handles both I and D perfectly so I recommend using this during development, you obviously need to keep this in mind when you go into production too as your users might have the same problem too. It might be an idea to save to disk first, provide a link to the pdf and then periodically purge your temp PDFs folder.</p>
<p>&nbsp;</p>
<p><em>*************<br />
$pdf-&gt;Cell(20,7,&rdquo;Rank&rdquo;,1,0,&rsquo;C',1);</em></p>
<p>which means create a cell of width 20 and height 7 with its value set to &ldquo;Rank&rdquo;. It should have a border, have its value centered and should have its background filled in.</p>
</body>
</html>