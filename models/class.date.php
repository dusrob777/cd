<?PHP
/*
getPreviousPeriodStart()
getPreviousPeriodEnd()
getCurrentPeriodStart()
getCurrentPeriodEnd() 
dateToday($cotr = "-")
dayToday()
dayLeft()
timeNow($cotr = ":" )
dateYesterday($cotr = "-")
getThisWeekMonday($cotr = "-")
getThisWeekSunday($cotr = "-")
getLastWeekMonday($cotr = "-")
getLastWeekSunday($cotr = "-")
adjustDays($adjDir,$date,$num,$cotr = "-")
adjustWeeks($adjDir,$date,$num,$cotr = "-")
adjustMonths($adjDir,$date,$num,$cotr = "-")
adjustYears($adjDir,$date,$num,$cotr = "-")
createDateArray($dateBegin,$dateEnd,$step = 1,$cotr ="-")
transferToInt($date)
getMondayByDate($date,$cotr = "-")
getFirstDayOfTheMonth($date,$cotr = "-")
getLastDayOfTheMonth($date,$cotr = "-")
timeToSec($time)
secToTime($seconds) 
timeToText($timenum)
convertDateFormat($dateformat='', $date='')
getHowLongAgo($date, $display = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'), $ago = 'Ago')
getTimeDifference($start_time,$end_time)
getDateDifference($start_date,$end_date,$return='days')
checkifExpired($date,$time)
checkifDateInRange($created)
*/
class DateHlp{

    const PERIOD_LENGTH     = 14;

    public static $now;
    public static $refStart;

    public static $currentMonth;
    public static $currentYear;

    public static $currPeriodStart;
    public static $currPeriodEnd;

    public static $prevPeriodStart;
    public static $prevPeriodEnd;
	
	private static $strTimeZone;
	
    public function __construct(){
		self::$strTimeZone = date_default_timezone_set('America/Chicago');
		
        self::$now                  = new DateTime();

        self::$currentMonth         = self::$now->format('m');
        self::$currentYear          = self::$now->format('Y');

        self::$refStart             = new DateTime("2015-01-02");

    }

    public function getPreviousPeriodStart() {

        $daysIntoCurrentPeriod      = ((int)self::$now->diff(self::$refStart)->format('%a') % self::PERIOD_LENGTH);

        self::$prevPeriodStart      = new DateTime('2 weeks ago');
        self::$prevPeriodStart->sub(new DateInterval('P'.$daysIntoCurrentPeriod.'D'));

        return self::$prevPeriodStart->format('Y-m-d');

    }

    public function getPreviousPeriodEnd() {

        $daysLeftCurrentPeriod      = self::PERIOD_LENGTH - ((int)self::$now->diff(self::$refStart)->format('%a') % self::PERIOD_LENGTH) - 1;

        self::$prevPeriodStart      = new DateTime('2 weeks ago');
        self::$prevPeriodStart->add(new DateInterval('P'.$daysLeftCurrentPeriod.'D'));

        return (self::$prevPeriodStart->format('Y-m-d'));

    }

    public function getCurrentPeriodStart() {

        $daysIntoCurrentPeriod      = (int)self::$now->diff(self::$refStart)->format('%a') % self::PERIOD_LENGTH;

        self::$currPeriodStart      = clone self::$now;
        self::$currPeriodStart->sub(new DateInterval('P'.$daysIntoCurrentPeriod.'D'));

        return (self::$currPeriodStart->format('Y-m-d'));


    }

    public function getCurrentPeriodEnd() {

        $daysLeftCurrentPeriod      = self::PERIOD_LENGTH - ((int)self::$now->diff(self::$refStart)->format('%a') % self::PERIOD_LENGTH) - 1;

        self::$currPeriodEnd        = clone self::$now;
        self::$currPeriodEnd->add(new DateInterval('P'.$daysLeftCurrentPeriod.'D'));

        return (self::$currPeriodEnd->format('Y-m-d'));

    }
	
    /*public function getThisWeekStart() {
		$day = clone self::$now;
		$day->setISODate((int)$day->format('o'), (int)$day->format('W'), 1);
		return $day->format('Y-m-d');
    }
	
	public function getThisWeekEnd() {
		$day = clone self::$now;
		$day->setISODate((int)$day->format('o'), (int)$day->format('W'), 7);
		return $day->format('Y-m-d');
    }
	
	public function getLastWeekStart(){
		$thisMonday = self::getThisWeekStart();		
		$lastMonday = $thisMonday->sub(new DateInterval('P14D'));
		return $lastMonday->format('Y-m-d');
    }*/
	
	public static function dateToday($cotr = "-"){
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		return	date($dateString);
		
	}
	/**
	 * Return So Called ISO-8601 day of the week, 1 for Monday 7 for Sunday
	 * A replacment for date("N") 
	 * @param string $strTz External Time zone if you want to bring it 
	 * @return int 1 to 7;
	 */
	public static function dayToday(){
		return date("N");
	}
	/**
	 * Return how many days left To "Next Monday", start counting from beginning of Today
	 * If Today is Monday , it still 7 days left 
	 * If Today is Sunday , still 1 day left
	 * @param string $strTz External Time zone if you want to bring it 
	 * @return int 1 to 7;
	 */
	public static function dayLeft(){
		return  8 - (int)date("N");
	}
	/**
	 * Return Current Time Using Format 23:59:59
	 * @param string $cotr default connector is ":" ,23:59:59
	 * @param string $strTz External Time zone if you want to bring it
	 * @return string  format 23:59:59  
	 */
	public static  function timeNow($cotr = ":" ){
		$timeString = "H" . $cotr . "i" . $cotr . "s";
		return  date($timeString);
	}
	/**
	 * Return The Date Of Yesterday
	 * @param string $cotr
	 * @param string $strTz External Time zone if you want to bring it 
	 * @return string Format "yyyy-mm-dd" 
	 */
	public static function dateYesterday($cotr = "-"){
		$dateToday = self::dateToday($cotr);
		$dateYesterday = self::adjustDays("sub", $dateToday,1,$cotr);

		return $dateYesterday;
	}
	/**
	 * Return Monday of Current Week
	 * If today is Monday, it will return the date of Today
	 * @param string $cotr
	 * @param string $strTz External Time zone if you want to bring it
	 * @return string Format "yyyy-mm-dd" 
	 */
	
	public static  function getThisWeekMonday($cotr = "-"){
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
			
		if(intval(date("w"))==1){
			$thisMonday = date($dateString,strtotime("this monday"));
		}
		else{
			$thisMonday = date($dateString,strtotime("last monday"));
		}		
		
		return $thisMonday; 
		
	}
	
	/**
	 * Return Sunday of Current Week
	 * @param string $cotr
	 * @param string $strTz External Time zone if you want to bring it
	 * @return string Format "yyyy-mm-dd" 
	 */
	public static  function getThisWeekSunday($cotr = "-"){
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		$thisMonday = self::getThisWeekMonday();
		$thisSunday = self::adjustDays("add",$thisMonday,6,$cotr);
		
		return $thisSunday;
	}
	/**
	 * Return Monday of Last Week
	 * @param string $cotr
	 * @param string $strTz External Time zone if you want to bring it
	 * @return string Format "yyyy-mm-dd" 
	 */
	public static  function getLastWeekMonday($cotr = "-"){
		$thisMonday = self::getThisWeekMonday();		
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		$lastMonday = self::adjustDays("sub",$thisMonday,7,$cotr);
		
		return $lastMonday;
	}
	/**
	 * Return Sunday of Last Week
	 * @param string $cotr
	 * @param string $strTz External Time zone if you want to bring it
	 * @return string Format "yyyy-mm-dd" 
	 */
	public static  function getLastWeekSunday($cotr = "-"){
		$thisMonday = self::getThisWeekMonday();
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		$lastSunday = self::adjustDays("sub", $thisMonday,1,$cotr);
		
		return $lastSunday;
	}	
	/**
	 * Adjust Dates 
	 * @param string $adjDir  Adjust Direction "add" or  "sub" ,please use lower case
	 * @param string $date  Date Start From 
	 * @param int $num   How many Days you want to adjust
	 * @param string $cotr connector
	 */	
	public static  function adjustDays($adjDir,$date,$num,$cotr = "-"){
		$dateResult = new DateTime($date);
		if($adjDir == "add"){
			$dateResult->add(new DateInterval('P' . $num . 'D'));		
		}
		elseif($adjDir == "sub"){
			
			$dateResult->sub(new DateInterval('P' . $num . 'D'));
		}
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		
		return $dateResult->format($dateString);
		
	}
	/**
	 *  Adjust Weeks
	 * @param string $adjDir  Adjust Direction "add" or  "sub" ,please use lower case
	 * @param string $date  Date Start From
	 * @param int $num   How many Weeks you want to adjust
	 * @param string $cotr connector
	 */	
	public static  function adjustWeeks($adjDir,$date,$num,$cotr = "-"){
		
		
		$dateResult = new DateTime($date);
		if($adjDir == "add"){
			$dateResult->add(new DateInterval('P' . $num*7 . 'D'));
		}
		elseif($adjDir == "sub"){
				
			$dateResult->sub(new DateInterval('P' . $num*7 . 'D'));
		}
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		
		return $dateResult->format($dateString);		
		
	}
	/**
	 *  Adjust Month
	 * @param string $adjDir  Adjust Direction "add" or  "sub" ,please use lower case
	 * @param string $date  Date Start From
	 * @param int $num   How many Months you want to adjust
	 * @param string $cotr connector
	 */	
	public static  function adjustMonths($adjDir,$date,$num,$cotr = "-"){
		
		
		$dateResult = new DateTime($date);
		if($adjDir == "add"){
			$dateResult->add(new DateInterval('P' . $num . 'M'));
		}
		elseif($adjDir == "sub"){
				
			$dateResult->sub(new DateInterval('P' . $num . 'M'));
		}
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		
		return $dateResult->format($dateString);
		
	}
	/**
	 *  Adjust Years
	 * @param string $adjDir  Adjust Direction "add" or  "sub" ,please use lower case
	 * @param string $date  Date Start From
	 * @param int $num   How many Years you want to adjust
	 * @param string $cotr connector
	 */	
	public static  function adjustYears($adjDir,$date,$num,$cotr = "-"){
		
		
		
		$dateResult = new DateTime($date);
		if($adjDir == "add"){
			$dateResult->add(new DateInterval('P' . $num . 'Y'));
		}
		elseif($adjDir == "sub"){
				
			$dateResult->sub(new DateInterval('P' . $num . 'Y'));
		}
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		
		return $dateResult->format($dateString);
		
	}
	/**
	 *  Create Data Array list By Steps
	 * @param string $dateBegin Date Array Start From
	 * @param string $dateEnd Date Array End 
	 * @param string $cotr  connector
	 * @return  array   array of date List 
	 */
	public static  function createDateArray($dateBegin,$dateEnd,$step = 1,$cotr ="-"){
		$dateArray = array();
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		$dateArray[]=date($dateString,strtotime($dateBegin));
		$dateResult = new DateTime($dateBegin);
		$dateComp = new DateTime($dateEnd);
		
		while($dateResult<$dateComp){
			$dateBegin = self::adjustDays("add", $dateBegin,$step,$cotr);
			$dateArray[] = $dateBegin;
			$dateBegin = str_replace($cotr,"-",$dateBegin);
			$dateResult = new DateTime($dateBegin);			
		}
		
		return $dateArray;
		
	}
	/**
	 * A Simple replaement of Date U with Time Zone Set
	 * @param unknown_type $date
	 * @return string
	 */
	public static function transferToInt($date){
		
		return date("U",strtotime($date));	
	}
	/**
	 * For Given Date , return Monday of that week
	 * @param string $date
	 * @param string $cotr
	 * @param string $strTz
	 * @return string 
	 */
	public static function getMondayByDate($date,$cotr = "-"){
		
		
		$dateString = "Y" . $cotr . "m" . $cotr . "d";
		$day = date("N",strtotime($date));
		
		$dayDiff = $day - 1;
		
		if( $dayDiff>0 ){
			$date = self::adjustDays("sub", $date,$dayDiff,$cotr);
		}
		
		return date($dateString,strtotime($date));
		
	}
	/**
	 * For Given Date , return First Day of that Month
	 * @param string $date
	 * @param string $cotr
	 * @param string $strTz
	 * @return string
	 */
	public static function getFirstDayOfTheMonth($date,$cotr = "-"){
		$dateString = "Y" . $cotr . "m" . $cotr . "01";
		return date($dateString,strtotime($date));	
	}
	/**
	 * For Given Date , return Last Day of the Month
	 * @param unknown_type $date
	 * @param unknown_type $cotr
	 * @param unknown_type $strTz
	 * @return string
	 */
	public static function getLastDayOfTheMonth($date,$cotr = "-"){
		$dateString = "Y" . $cotr . "m" . $cotr . "t";
		return date($dateString,strtotime($date));
	
	}
	/**
	 * Convert Time to Seconds
	 *
	*/
	public static function timeToSec($time) { 
		$hours = substr($time, 0, -6); 
		$minutes = substr($time, -5, 2); 
		$seconds = substr($time, -2); 
		return $hours * 3600 + $minutes * 60 + $seconds; 
	}
	/**
	 * Convert Seconds to Time
	 *
	*/
	public static function secToTime($seconds) { 
		$hours = floor($seconds / 3600); 
		$minutes = floor($seconds % 3600 / 60); 
		$seconds = $seconds % 60; 
		return sprintf("%d:%02d:%02d", $hours, $minutes, $seconds); 
	} 
	
	/**
	 * Convert Time to Text
	 *
	*/	
	public static function timeToText($timenum){
		list($h1,$m1,$s1)=split(":",$timenum);
		$ret ='';
		if ($h1 !=0){
			if ($h1 == 1)
				$ret .= $h1." hour ";
			else 
				$ret .= $h1." hours ";
		}
		if ($m1 !=0) {
			if ($m1 == 1)
				$ret .= $m1." minute ";
			else 	
				$ret .= $m1." minutes ";
		}
		if ($s1 !=0) {
			if ($m1 == 1)
				$ret .= $s1." second ";
			else
				$ret .= $s1." seconds ";
		}
		if ($h1 ==0 && $m1 ==0 && $s1 ==0) {
			$ret .= "Nothing yet";	
		}
		return $ret;
	}
	/*
	* //usage
	* //ConvertDateFormats("m/d/Y", $date)
	*/
	public static function convertDateFormat($dateformat='', $date='')
	{
		if ($date =='')
			return date($dateformat);
		else			
			return date($dateformat,strtotime($date));
	}
	/**
	 * How long ago 
	 *
	*/
	public static function getHowLongAgo($date, $display = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'), $ago = 'Ago')
	{
		$date = getdate(strtotime($date));
			$current = getdate();
			$p = array('year', 'mon', 'mday', 'hours', 'minutes', 'seconds');
			$factor = array(0, 12, 30, 24, 60, 60);
		
			for ($i = 0; $i < 6; $i++) {
				if ($i > 0) {
					$current[$p[$i]] += $current[$p[$i - 1]] * $factor[$i];
					$date[$p[$i]] += $date[$p[$i - 1]] * $factor[$i];
				}
				if ($current[$p[$i]] - $date[$p[$i]] > 1) {
					$value = $current[$p[$i]] - $date[$p[$i]];
					return $value . ' ' . $display[$i] . (($value != 1) ? 's' : '') . ' ' . $ago;
				}
			}
		
			return '';
	}

	/**
	 *
	 *
	*/
	public static function getTimeDifference($start_time,$end_time){
		$start_time = new DateTime($start_time);
		$end_time = new DateTime($end_time);
		$interval = $start_time->diff($end_time);
		$hours   = $interval->format('%h'); 
		$minutes = $interval->format('%i');
		$ret ='';
		if ( $hours != 0){
			$ret .= ($hours == 1) ? $hours.' hour ' : $hours.' hours ';
		} else {
			$ret .='';
		}
		if ( $minutes != 0){
			$ret .= ($minutes == 1) ? $minutes.' min ' : $minutes.' mins ';
		} else {
			$ret .= '';
		}
		return $ret;
	}
	/**
	 *
	 *
	*/
	public static function getDateDifference($start_date,$end_date,$return='days'){
		$start_date = new DateTime($start_date);
		$end_date = new DateTime($end_date);
		$interval = $start_date->diff($end_date);
		if ($return == 'days'){
			return ($interval->format("%a") == 1) ? $interval->format("%a").' day' : $interval->format("%a").' days';
		} else if ($return == 'years'){
			return ($interval->format("%y") == 1) ? $interval->format("%y").' year' : $interval->format("%y").' years';
		} else if ($return == 'months'){
			return ($interval->format("%m") == 1) ? $interval->format("%m").' month' : $interval->format("%m").' months';
		}
	}	
	/**
	 *
	 *
	*/
	public static function checkifExpired($date,$time){
	   $exp = explode(":",$time);
	   $date = new DateTime($date);
	   $interval = 'PT'.$exp[0].'H'.$exp[1].'M';
	   $date->add(new DateInterval($interval));
	   $mydate = $date->format('Y-m-d  H:i') ;
	   if (strtotime($mydate) <= time()){
			return true; 
	   } else {
		   return false;
	   }
	}
	/**
	 *	
	 *
	*/
	public static function checkifDateInRange($created){
		if (($created >= self::getCurrentPeriodStart()) && ($created <= self::getCurrentPeriodEnd())){
				return true;
		} else {
				return false;
		}
	}	
	/**
	 * inLine Function To Setting The Time Zone
	 * @param string $strTz External Time zone if you want to bring it
	 
	private static function timeZoneSet(){
		$strTimeZoneChoice = ($strTz == null)?self::strTimeZone:$strTz;
		date_default_timezone_set($strTimeZoneChoice);
	}*/
}?>