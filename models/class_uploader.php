<?PHP
//USAGE
/*
<form encType="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <input name="myFile" type="file">
  <br>
  <input name="mySubmit" value="Upload" type="submit">
</form>


if(isset($_POST['mySubmit'])){
  include 'easyUp.php';
  $up = new fileDir('/myUploads/');
	
  $up->upload($_FILES['myFile']);
}

//DELETE FILE
include 'easyUp.php';
$up = new fileDir('/myUploads/');
	
$up->delete('myPic.jpg');

//location() and fileName() retrieve information about the last file uploaded.
if(isset($_POST['mySubmit'])){
  include 'easyUp.php';
  $up = new fileDir('/myUploads/');
	
  $up->upload($_FILES['myFile']);

  echo $up->location();
  // outputs: yourServersAbsolutePath/myUploads/
  
  echo $up->fileName();
  // outputs: myPic.jpg if our last uploaded file was named myPic.jpg.
}

*/
class fileDir {
  private $fileInfo;
  private $fileLocation;
  private $error;
  private $direct;
  public $max_size = 500000000; // Maximum file size 3mb
  function __construct($dir){
	  $this->direct = $dir;//$_SERVER['DOCUMENT_ROOT'].$dir;
	  if(!is_dir($this->direct)){
		  mkdir($this->direct);
		 // die('Supplied directory is not valid: '.$this->direct);	
	  }
  }
  function upload($theFile){
	  $this->fileInfo = $theFile;
	  $this->fileLocation = $this->direct . $this->fileInfo['name'];
	   if(!file_exists($this->fileLocation)){
			if ($this->fileInfo['name']['size'] > $this->max_size) {
				return "The file size is over 5MB.";
			}else {
				$extensions = array('jpeg','jpg','png','doc','docx','xls','xlsx','pdf','gif','bmp','txt');
				$fextn = pathinfo($this->fileInfo['name'], PATHINFO_EXTENSION);
				if(in_array($fextn,$extensions)) {
					if(move_uploaded_file($this->fileInfo['tmp_name'], $this->fileLocation)){
						return 'File was successfully uploaded';
					} else {
						return 'File could not be uploaded';
						$this->error = "Error: File could not be uploaded.\n";
					}
				} else {
					return 'File extension is not allowed';
				}
			}
	  } else {
		  return 'File name already exists';	
	  }
  }
  function overwrite($theFile){
	  $this->fileInfo = $theFile;
	  $this->fileLocation = $this->direct . $this->fileInfo['name'];
	  if(file_exists($this->fileLocation)){
		  $this->delete($this->fileInfo['name']);
	  }
	  return $this->upload($this->fileInfo);
  }
  function location(){
	  return $this->fileLocation;	
  }
  function fileName(){
	  return $this->fileInfo['name'];
  }
  function delete($fileName){
	  $this->fileLocation = $this->direct.$fileName;
	  if(is_file($this->fileLocation)){
		unlink($this->fileLocation);
		return 'Your file was successfully deleted';
	  } else {
		return 'No such file exists: '.$this->fileLocation;	
	  }
  }
  function reportError(){
	  return $this->error;	
  }
}
?>