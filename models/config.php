<?php
require_once("db-settings.php"); //Require DB connection

//Retrieve settings
$stmt = $mysqli->prepare("SELECT id, name, value
	FROM ".$db_table_prefix."configuration");	
$stmt->execute();
$stmt->bind_result($id, $name, $value);

while ($stmt->fetch()){
	$settings[$name] = array('id' => $id, 'name' => $name, 'value' => $value);
}
$stmt->close();

//Set Settings
$emailActivation = $settings['activation']['value'];
$mail_templates_dir = "models/mail-templates/";
$websiteName = $settings['website_name']['value'];
$websiteUrl = $settings['website_url']['value'];
$emailAddress = $settings['email']['value'];
$resend_activation_threshold = $settings['resend_activation_threshold']['value'];
$emailDate = date('dmy');
$language = $settings['language']['value'];
$template = $settings['template']['value'];

$master_account = -1;

$default_hooks = array("#WEBSITENAME#","#WEBSITEURL#","#DATE#");
$default_replace = array($websiteName,$websiteUrl,$emailDate);

//$directory = realpath();
$directory = (dirname(__FILE__));

if (!file_exists($language)) {
	$language = $directory."/languages/en.php";
}
//
$dateformat					= "m/d/Y g:i a";
$longdatetime				= "M j, Y, g:i a"; //Nov 17, 2011, 2:30 pm

//
if(!isset($language)) $language = $directory."/languages/en.php";

// MODULE Configuration \\
$module_base_dir 			= 'modules/'; //Directory of modules (You can use absolute path)
$module_ext 				= '.php'; // Our Module Extension
$module_base_file			= $module_ext; // So this will show HelloWorld.module.php
//
//$uploaddir					= "C:\Users\hakan\OneDrive\WWW\SharedProjects\applications\helpdesk\upload";
$uploaddir	="C:\inetpub\wwwroot\helpdesk\uploads";
//$uploaddir1="C:\inetpub\wwwroot\helpdesk\uploads";

define("GE_UPLOAD",$uploaddir);
define("GE_SITE",$websiteUrl);
define("ROOT",realpath($_SERVER['DOCUMENT_ROOT'].'/codedirectory'));
//Pages to require
//require_once($language);
require_once("class.database.php");
require_once("class.user.php");
require_once("class.newuser.php");
require_once("funcs.php");
require_once("class.date.php");
require_once("class.upload.php");
require_once("customfunctions.php");
require_once("class_uploader.php");



$db= new HelpDatabase();

session_start();
$date= new DateHlp();
//Global User Object Var
//loggedInUser can be used globally if constructed
if(isset($_SESSION["idealdesksession"]) && is_object($_SESSION["idealdesksession"]))
{
	$loggedInUser = $_SESSION["idealdesksession"];
}

?>
