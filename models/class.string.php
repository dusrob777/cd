<?PHP

class StringHlp{
	
    public function __construct(){

    }
    /**
     * Validates whether the given element is a string.
     *
     * @return bool
     * @param string $element
     * @param bool $require_content If the string can be empty or not
     */
    public static function validateString($element, $require_content = true)
    {
        return (!is_string($element)) ? false : ($require_content && $element == '' ? false : true);
    } 	
	/*
	 * mytruncate 
	 *
	*/
	public static function mytruncate($string, $limit, $break=" ", $pad="...")
	{
	  // return with no change if string is shorter than $limit
	  if(strlen($string) <= $limit) return $string;
	
	  $string = substr(strip_tags($string), 0, $limit);
	  if(false !== ($breakpoint = strrpos($string, $break))) {
		$string = substr($string, 0, $breakpoint);
	  }
	  return $string . $pad;
	}	
	/*
	 * replaceLineBreaks
	 *
	*/
	public static function replaceLineBreaks($text){
		return str_replace( array("\n","\r","\r\n"), '<br />', $text );
	}
	
	/**
     * Get the ordinal value of a number (1st, 2nd, 3rd, 4th).
     *
     * @return string
     * @param int $value
     */ 
    public static function getOrdinalString($value)
    {
        static $ords = array('th', 'st', 'nd', 'rd');
        if ((($value %= 100) > 9 && $value < 20) || ($value %= 10) > 3) {
            $value = 0;
        }
        return $ords[$value];
    } 
	
	/**
     * Returns the plural appendage, handy for instances like: 1 file,
     * 5 files, 1 box, 3 boxes.
     *
     * @return string
     * @param int $value
     * @param string $append what value to append to the string
     */
    public static function getPluralString($value, $append = 's')
    {
        return ($value == 1 ? '' : $append);
    } 
	
    /**
     * Counts number of words in a string.
     *
     * if $real_words == true then remove things like '-', '+', that
     * are surrounded with white space.
     *
     * @return string|null
     * @param string $string
     * @param bool $real_words
     */
    public static function countWords($string, $real_words = true)
    {
        if (StringHlp::validateString($string)) {
            if ($real_words == true) {
                $string = preg_replace('/(\s+)[^a-zA-Z0-9](\s+)/', ' ', $string);
            }
            return (count(split('[[:space:]]+', $string)));
        } else {
            return null;
        }
    } 	
    /**
     * Counts number of sentences in a string.
     *
     * @return string|null
     * @param string $string
     */
    function countSentences($string)
    {
        if (StringHlp::validateString($string)) {
            return preg_match_all('/[^\s]\.(?!\w)/', $string, $matches);
        } else {
            return null;
        }
    } 	
}?>