<?php

/**
 * @author Jooria Refresh Your Website <www.jooria.com>
 * @copyright 2010
 */
//USAGE
/*	$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
	$page = ($page == 0 ? 1 : $page);
	$perpage = 2;//limit in each page
	$startpoint = ($page * $perpage) - $perpage;


$sql = "SELECT * FROM projects where User_ID=".$loggedInUser->user_id." ORDER BY Created DESC LIMIT $startpoint,$perpage";
*/

//Pages("table","limit to appear","the path", userid);
function Pages($tbl_name,$limit,$path,$parameter=NULL)
{
	global $mysqli;
	$query = "SELECT * FROM $tbl_name $parameter";
	$mysqli->query($query);
	$total_pages = $mysqli->affected_rows;
	$adjacents = "2";
	//echo $total_pages."<BR>";
	// echo $query."<BR>";
	$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
	$page = ($page == 0 ? 1 : $page);

	if($page)
	$start = ($page - 1) * $limit;
	else
	$start = 0;

	$sql = "SELECT * FROM $tbl_name $parameter LIMIT $start, $limit";
	$mysqli->query($sql);
	//echo $sql;
	$prev = $page - 1;
	$next = $page + 1;
	$lastpage = ceil($total_pages/$limit);
	$lpm1 = $lastpage - 1;

	$pagination = "";
    if($lastpage > 1)
    {   
    if ($page > 1)
    	$pagination.= "<a href='".$path."page=$prev' class='btn'><i class='fa fa-angle-left'></i></a>";
    else
    	$pagination.= "<a href='#' class='btn'><i class='fa fa-angle-left'></i></a>";   
        if ($lastpage < 7 + ($adjacents * 2))
        {   
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                	$pagination.= "<a href='#' class='btn active btn-blue'>$counter</a>";
                else
                	$pagination.= "<a href='".$path."page=$counter' class='btn'>$counter</a>";     
                         
            }
        }elseif($lastpage > 5 + ($adjacents * 2)){
        if($page < 1 + ($adjacents * 2)){
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
            if ($counter == $page)
            	$pagination.= "<a href='#' class='btn active btn-blue'>$counter</a>";
            else
            	$pagination.= "<a href='".$path."page=$counter' class='btn'>$counter</a>";     
                         
            }
        	$pagination.= "<a href='#' class='btn'>...</a>";
        	$pagination.= "<a href='".$path."page=$lpm1' class='btn'>$lpm1</a>";
        	$pagination.= "<a href='".$path."page=$lastpage' class='btn'>$lastpage</a>";   
           
        }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
        	$pagination.= "<a href='".$path."page=1' class='btn'>1</a>";
        	$pagination.= "<a href='".$path."page=2' class='btn'>2</a>";
        	$pagination.= "<a href='#' class='btn'>...</a>";
        for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
        {
        if ($counter == $page)
        	$pagination.= "<a href='#' class='btn active btn-blue'>$counter</a>";
        else
        	$pagination.= "<a href='".$path."page=$counter' class='btn'>$counter</a>";     
                     
        }
        	$pagination.= "<a href='#' class='btn'>...</a>";
        	$pagination.= "<a href='".$path."page=$lpm1' class='btn'>$lpm1</a>";
        	$pagination.= "<a href='".$path."page=$lastpage' class='btn'>$lastpage</a>";   
           
        }else{
        	$pagination.= "<a href='".$path."page=1' class='btn'>1</a>";
        	$pagination.= "<a href='".$path."page=2' class='btn'>2</a>";
        	$pagination.= "<a href='#' class='btn'>...</a>";
        for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
        {
        if ($counter == $page)
        	$pagination.= "<a href='#'  class='btn active btn-blue'>$counter</a>";
        else
        	$pagination.= "<a href='".$path."page=$counter' class='btn'>$counter</a>";     
                     
        }
        }
    }
    if ($page < $counter - 1)
    	$pagination.= "<a href='".$path."page=$next'  class='btn'><i class='fa fa-angle-right'></i></a>";
    else
    	$pagination.= "<a href='#' class='btn'><i class='fa fa-angle-right'></i></a>";
    }

	if (empty($pagination))
		$pagination = "&nbsp;";
		return $pagination;
	}
?>
