<?php 
//usage:
/*
$upfile= new ObjUpload('upfile'); // upfile is form file field name 
$upfile->setExtensions('jpeg,jpg,png,gif,flv'); // Permitted file extensions
// upload folder,filename,uniquenamesetting,optional width (For images), Optional height (For Images), maximum filesize 
$upfile->upload($SET_Upload_Folder,null,true,200,200,1048576);  

echo $upfile->upResult;  

to make a custom size, uncomment below line
//$this->makeCustomImg($f_tmpname,$newFileName,$f_type,$fwidth,$fheight);
*/
  class ObjUpload{
      
      var $inputName; 
      var $fileName;  
      var $tmpName;   
      var $type;      
      var $size;      
      var $sizeAsKb;  
      var $error;     
      var $fileExtn;  
      var $permitExtn=Array(); 
      var $upResult;   
	  var $changedfilename;  
      
      function ObjUpload($inputName)
      {   
               $upObjFile=$_FILES[$inputName]; 
               $this->inputName=$inputName;
               $this->fileName=$upObjFile['name'];
               $this->tmpName=$upObjFile['tmp_name'];
               $this->type=$upObjFile['type'];
               $this->size=$upObjFile['size'];
               $this->error=$upObjFile['error'];
               $this->fileExtn=end(explode('.',$this->fileName)); 
      }                 
      function upload($dir,$upfile_adi=False,$unique=False,$fwidth,$fheight,$fmax_size)
      {           
            $this->error=is_null($this->fileName) ? 0 : 4;
            $this->error=in_array($this->fileExtn,$this->permitExtn) ? 0 : 99;
            $this->error=is_dir($dir)? $this->error : 98;     
            $this->error=is_writable($dir) ? $this->error : 97;             
                             
            switch($this->error)
            { 
                  case 0:                
                    $chk_format    =    $this->is_typesupported($this->fileExtn,$this->type);
                    if($chk_format==0)
                    {
                        $this->upResult    =    "Invalid File Format : Upload Failed";
                    }
                    else
                    {
                        if($this->size > $fmax_size)
                        {
                        	$this->upResult    =    "File Size Exceeded : Upload Failed";
                        }
                        else
                        {
							$upfile_son_adi    =    $unique ? time().'.'.$this->fileExtn : ($upfile_adi ? $upfile_adi.'.'.$this->fileExtn : $this->fileName);
							$this->processUpload($this->tmpName,$dir.'/'.$upfile_son_adi,$this->type,$this->fileExtn,$fwidth,$fheight);
							$this->upResult    =    $upfile_son_adi." Uploaded";
							if ($unique)
								$this->changedfilename = $upfile_son_adi;
							else
								$this->changedfilename = $upfile_adi;
                        }
                    }                
                    return $this;
                  case 4: //File not Selected: 
                    return $this->resultProcess($this->error);
                    break;
                case 99: //File not Supported: 
                    return $this->resultProcess($this->error);
                    break;
                case 98: //$dir.' Not a Folder'
                    return $this->resultProcess($this->error);
                    break;
                case 97: //$dir.' Not a Writable Folder'
                      return $this->resultProcess($this->error);
                    break;
                  default: //'Unknown Error: '.
                    return $this->resultProcess($this->error); 
                      break;            
          }   
      }
      
      
      function setExtensions($extnlist)
      {
           $this->permitExtn=explode(',',$extnlist); 
           return $this;
      }
         
      function resultProcess($upError)
      {
          die($upError); 
      }
      //File type cross checking function - with mime types
      //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      function is_typesupported($fextn,$ftype)
      {
          
        $frmt_return        =    0;
        if($fextn=='jpeg' || $fextn=='pjpeg' || $fextn=='jpg' || $fextn=='gif' || $fextn=='png')
        {            
            $ftype_array    =    array("image/pjpeg","image/jpeg","image/gif","image/x-png","image/png");            
            if(in_array($ftype,$ftype_array))
            {
            $frmt_return    =    1;
            }            
        }                
        
        if($fextn=='flv')
        {
            $ftype_array    =    array("application/octet-stream","video/x-flv");    
            if(in_array($ftype,$ftype_array))
            {
            $frmt_return    =    1;
            }
        }        
        return $frmt_return;
      }
     //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
     
     function processUpload($f_tmpname,$newFileName,$f_type,$f_extn,$fwidth,$fheight)
     {
         
        if($f_extn=='jpeg' || $f_extn=='pjpeg' || $f_extn=='jpg' || $f_extn=='gif' || $f_extn=='png')
        {
        	copy($f_tmpname,$newFileName);
        //$this->makeCustomImg($f_tmpname,$newFileName,$f_type,$fwidth,$fheight);
        }
        if($f_extn=='flv')
        {
        	copy($f_tmpname,$newFileName);
        }
     }
     
     function makeCustomImg($f_tmpname, $newFileName, $f_type, $new_w, $new_h)
     {
             $overlay = '';
            $trans_r = 255;
            $trans_g = 255;
            $trans_b = 255;
            
                switch($f_type) 
                {
                    case 'image/jpeg':
                        $orig = imagecreatefromjpeg($f_tmpname);
                        break;
                    case 'image/jpg':
                        $orig = imagecreatefromjpeg($f_tmpname);
                        break;
                    case 'image/pjpeg':
                        $orig = imagecreatefromjpeg($f_tmpname);
                        break;
                    case 'image/png':
                        $orig = imagecreatefrompng($f_tmpname);
                        break;
                    case 'image/gif':
                        $orig = imagecreatefromgif($f_tmpname);
                        break;
                    case 'image/wbmp':
                        $orig = imagecreatefromwbmp($f_tmpname);
                        break;
                    default:
                        $error = 'Unknown File Format or MIME Type';
                        $show = 'error';
                        $flag = 5;
                }
                    
                if($orig)
                {
                    $old_x    =    imageSX($orig);
                    $old_y    =    imageSY($orig);
                    if ($old_x > $old_y) 
                    {
                        $large_x=$new_w;
                        $large_y=$old_y*($new_h/$old_x);
                    }
                        
                    if ($old_x < $old_y) 
                    {
                        $large_x=$old_x*($new_w/$old_y);
                        $large_y=$new_h;
                    }
                        
                    if ($old_x == $old_y) 
                    {
                        $large_x=$new_w;
                        $large_y=$new_h;
                    }
                    
                    $large = imagecreatetruecolor($large_x, $large_y);     
					imagecopyresampled($large, $orig, 0, 0, 0, 0, $large_x, $large_y,$old_x,$old_y);
                                                
                    imagejpeg($large, $newFileName,80);
                }
    }     
      
  }
  ?> 