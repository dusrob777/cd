<?PHP

	function displaylatestTickets(){
		
	}
	
	function insertNotifications($text,$itemid,$action='info',$ownerid='',$agent=''){
			global $mysqli,$loggedInUser;
			$ownerid = !empty($ownerid) ? $ownerid : $loggedInUser->user_id;
			$sql = "INSERT INTO `notifications` (
						`user_id`,`text`,`created` ,`issueid`,`action`)
					VALUES (
						'".$ownerid."', '".$text."', '".date("Y-m-d H:i:s")."','".$itemid."','".$action."');";
			$mysqli->query($sql);
			if ($agent !=''){
				$nextid = $mysqli->insert_id;
				$query = "INSERT INTO `notifications_items` (
							`notify_id`,`sent_to`,`displayed`)
						VALUES (
							'".$nextid."', '".$agent."', '0');";
				$mysqli->query($query);
			}
	}
	/*
	
	*/
	function initials($str) {
		$ret = '';
		foreach (explode(' ', $str) as $word)
			$ret .= strtoupper($word[0]);
		return $ret;
	}

	function getIssueTypeRadio($id){
			global $mysqli;
			$sql = "SELECT * FROM helpdesk_issue_types ORDER BY issue_type ASC";
			$ret ='';
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['issue_type_id'] == $id)
					$sel = " checked=\"checked\"";	
				else
					$sel ="";
				$ret .='<div class="control-group">
						  <div class="radio">
							<label>
							  <input name="form-field-radio" class="colored-blue" type="radio" value="'.$nt['issue_type_id'].'" '.$sel.'>
							  <span class="text"> '.$nt['issue_type'].'</span> </label>
						  </div>
						</div>
';
			}
			return $ret;

	}	
	function getIssueType($id){
			global $mysqli;
			$sql = "SELECT * FROM helpdesk_issue_types ORDER BY issue_type ASC";
			echo "<select name=\"issue_type\" id=\"issue_type\" class=\"form-control\">";
			if ($id == '')
				echo "<option value=\"\" selected=\"selected\"></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['issue_type_id'] == $id)
						$sel = " selected=\"selected\"";	
				else
						$sel ="";
				echo "<option value=\"".$nt['issue_type_id']."\" $sel>".$nt['issue_type']."</option>";
			}
			echo "</select>";

	}

	function removeAllSpecialChars($text){
		return str_replace( array("(",")","-","."), '', $text );
	}
	//
	function replaceLineBreaks($text){
		return str_replace( array("\n","\r","\r\n"), '<br />', $text );
	}
	
	function is_ajax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	} 
	//
	/*
	*/
	function sendEmail($touseremailarray,  $senderid, $subject,$emailbody=''){
		global $SETTINGS_SMTP_Host, $SETTINGS_SMTP_Port, $SETTINGS_NoReplyEmail,$loggedInUser, $emailreplyto,$SETTINGS_SMTP_username,$SETTINGS_SMTP_pass;
		$response;
		require_once(realpath(dirname(dirname(__FILE__)))."/lib/PHPMailer/class.phpmailer.php");
		$mail = new PHPMailer(); 
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPAuth   	= true;
		$mail->Host       	= $SETTINGS_SMTP_Host; // SMTP server
		$mail->Port       	= $SETTINGS_SMTP_Port;  
		$mail->Username   	= $SETTINGS_SMTP_username;
		$mail->Password   	= $SETTINGS_SMTP_pass;        // SMTP account password
		$mail->From     	= $SETTINGS_NoReplyEmail;
		$mail->FromName 	= 'Idyllic Intranet System';
		$mail->Sender 		= $SETTINGS_NoReplyEmail;
		$mail->AddReplyTo($loggedInUser->email, $emailreplyto);

		foreach ($touseremailarray as $val) {
			if ($senderid != $val){
				$to_email 		= GetNamesFromID('users','email','','id',$val);
				$to_username	= GetNamesFromID('users','display_name','','id',$val);
				$mail->AddAddress($to_email, $to_username);
			}
		}
		$mail->Subject = $subject;
	    $mail->MsgHTML($emailbody);
		$mail->WordWrap = 50;
		if(!$mail->Send()) {
			  $response .= 'Email not sent.<BR />';
			  $response .= 'Mail error: ' . $mail->ErrorInfo.'.<BR />';
			  return $response;
		 } 
		 return true;
	}

	
	function checkIfUserHasPM($pm_id){
		global $mysqli,$loggedInUser;
		$sql = "SELECT * FROM project_meeting_invitees WHERE
				pm_id = ".$pm_id." and user_id=".$loggedInUser->user_id." AND dismiss=0 LIMIT 1";
		$result = $mysqli->query($sql);	
		$count = $result->num_rows;
		if($count > 0) 
			return true;
		else
			return false;	
	}
	function getUserPicture($class='',$width=128,$height=128,$userid=''){
		global $mysqli,$loggedInUser;
		$displayclass='';
		if ($userid != ''){
			$user = $userid;
		} else {
			$user = $loggedInUser->user_id;
		}
		$sql = "SELECT picture,gender FROM users where id=".$user;
		$results=$mysqli->query($sql);		
		$row=$results->fetch_array();
		$ret;
		if ($class !=''){
			$displayclass= ' class="'.$class.'"';
		} else {
			$displayclass='';
		}
		if($row['picture'] !='') {
		   		$ret = '<img src="upload/employeepictures/'.$row['picture'].'" '.$displayclass.'  width="'.$width.'" height="'.$height.'" alt="pictures">';
	    } else {
		   	if($row['gender'] =='Male') {
		   		$ret = '<img src="img/pictures/male-64.png" '.$displayclass.'  width="'.$width.'" height="'.$height.'" alt="pictures">';
			} else if($row['gender'] =='Female') {
				$ret = '<img src="img/pictures/female-64.png" '.$displayclass.'  width="'.$width.'" height="'.$height.'" alt="pictures">';
			}else {
				$ret = '<img src="img/pictures/female-64.png" '.$displayclass.'  width="'.$width.'" height="'.$height.'" alt="pictures">';
			}
	    } 
		return $ret;
	}
	
	function mytruncate($string, $limit, $break=" ", $pad="...")
	{
	  // return with no change if string is shorter than $limit
	  if(strlen($string) <= $limit) return $string;
	
	  $string = substr(strip_tags($string), 0, $limit);
	  if(false !== ($breakpoint = strrpos($string, $break))) {
		$string = substr($string, 0, $breakpoint);
	  }
	
	  return $string . $pad;
	}	

	function getHowLongAgo($date, $display = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'), $ago = 'Ago')
	{
		$date = getdate(strtotime($date));
			$current = getdate();
			$p = array('year', 'mon', 'mday', 'hours', 'minutes', 'seconds');
			$factor = array(0, 12, 30, 24, 60, 60);
		
			for ($i = 0; $i < 6; $i++) {
				if ($i > 0) {
					$current[$p[$i]] += $current[$p[$i - 1]] * $factor[$i];
					$date[$p[$i]] += $date[$p[$i - 1]] * $factor[$i];
				}
				if ($current[$p[$i]] - $date[$p[$i]] > 1) {
					$value = $current[$p[$i]] - $date[$p[$i]];
					return $value . ' ' . $display[$i] . (($value != 1) ? 's' : '') . ' ' . $ago;
				}
			}
		
			return '';
	}

	/*
	
	*/
	function companyDropDown($id=''){
			global $mysqli;
			$sql = "SELECT companyid, company_name FROM company where active=1 and deleted=0 ORDER BY company_name ASC";
			echo "<select name=\"company_id\" id=\"company_id\" class=\"select\">";
			if ($id == '')
				echo "<option value=\"\" selected=\"selected\"></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['companyid'] == $id)
						$sel = " selected=\"selected\"";	
				else
						$sel ="";
				echo "<option value=\"".$nt['companyid']."\" $sel>".$nt['company_name']."</option>";
			}
			//echo "</select>";
	}
	
	/*
	
	*/
	function usergroupDropDown($id=''){
			global $mysqli;
			$sql = "SELECT id, name FROM user_groups ORDER BY name ASC";
			echo "<select name=\"permission_id\" id=\"permission_id\" class=\"select\">";
			if ($id == '')
				echo "<option value=\"\" selected=\"selected\"></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['id'] != 11){
					if ($nt['id'] == $id)
							$sel = " selected=\"selected\"";	
					else
							$sel ="";
					echo "<option value=\"".$nt['id']."\" $sel>".$nt['name']."</option>";
				}
			}
			echo "</select>";
	}
	/*
	
	*/
	function userAgentDropDown($id=''){
			global $mysqli;
			$sql = "SELECT id, display_name FROM users where active =1 AND deleted=0 AND support_agent=1 ORDER BY display_name ASC";
			echo "<select name='user_id' id='user_id' class='select2' style='width:300px'>";
			//if ($id =='')
				echo "<option value=''></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['id'] == $id)
						$sel = " selected='selected'";	
				else
						$sel ="";
				echo "<option value='".$nt['id']."' $sel>".$nt['display_name']."</option>";
			}
			echo "</select>";
	}
	/*
	
	*/
	function getFileTypeFlat($extn) {
    switch ($extn) {
        case "doc":
        case "docx":
            return '<i class="icon-file-word icon-2x"></i> ';
            break;
        case "png":
        case "PNG":
        case "jpg":
        case "JPG":
            return '<i class="icon-image2 icon-2x"></i> ';
            break;
        case "csv":
        case "xls":
        case "xlsx":
            return '<i class="icon-file-excel icon-2x"></i> ';
            break;
        case "pdf":
            return '<i class="icon-file-pdf icon-2x"></i> ';
            break;
        case "txt":
            return '<i class="icon-file-text icon-2x"></i> ';
            break;
        default:
            return '<i class="icon-file-empty icon-2x"></i> ';
            break;
           
    }
}

function getSize($size, $precision = 2, $long_name = false, $real_size = true){
    $base = $real_size ? 1024 : 1000;
    $pos = 0;
    while ($size > $base) {
        $size /= $base;
        $pos++;
    }
    $prefix = getSizePrefix($pos);
    $size_name = ($long_name) ? $prefix . "bytes" : $prefix[0] . "B";
    return round($size, $precision) . ' ' . ucfirst($size_name);
}
function formatSize( $bytes )
{
        $types = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $types ) -1 ); $bytes /= 1024, $i++ );
                return( round( $bytes, 2 ) . " " . $types[$i] );
}      
    /**
    * Filemanager::_getSizePrefix()
    *
    * @param mixed $pos
    * @return
    */
function getSizePrefix($pos)
{
    switch ($pos) {
        case 00:
            return "";
        case 01:
            return "kilo";
        case 02:
            return "mega";
        case 03:
            return "giga";
        default:
            return "?-";
        }
}

function getFileType($extn) {
          
        switch ($extn) {
            case "ai":
                return "ai.png";
                break;
                
            case "css":
                return "css.png";
                break;
                
            case "csv":
            case "xlsx":
            case "xls":
                return "excel.png";
                break;
                
            case "fla":
            case "swf":
                return "fla.png";
                break;
                
            case "mp3":
            case "wav":
                return "wav.png";
                break;
                
            case "gif":
            case "png":
                return "png.png";
                break;
            case "jpg":
            case "JPG":
                return "jpg2.png";
                break;
                    
            case "bmp":
            case "dib":
                return "bmp.png";
                break;
                
            case "txt":
            case "log":
            case "sql":
                return "text.png";
                break;
                
            case "js":
                echo "jscript.png";
                break;
            
            case "pdf":
                return "pdf.png";
                break;
                
            case "zip":
            case "tgz":
            case "gz":
                return "zip.png";
                break;
            case "rar":    
            return "rar.png";
                break;
            case "doc":
            case "rtf":
                return "word.png";
                break;
            case "docx":    
                return "word_docx.png";
                break;
                
            case "asp":
            case "jsp":
                echo "asp.png";
                break;
                
            case "php":
                return "desktop.png";
                break;
                
            case "htm":
            case "html":
                return "html.png";
                break;
                
            case "ppt":
                return "powerpoint.png";
                break;
                
            case "exe":
            case "com":
                return "exe.png";
                break;
            case "bat":    
                return "bat.png";
                break;            
                
            case "wmv":
                return "wmv.png";
                    break;
            case "mpg":
            case "mpeg":
            case "wma":
            case "asf":
                return "mpg.png";
                break;
                
            case "midi":
            case "mid":
                return "mpg.png";
                break;
                
            case "mov":
                return "mov.png";
                break;
                
            case "psd":
                return "psd.png";
                break;
                
            case "ram":
            case "rm":
                return "real.png";
                break;
                
            case "xml":
                return "xml.png";
                break;
                
            default:
                return "default.png";
                break;
        }    

      }


	/*
	
	*/
	
	
	
	
	
	
	

	function checkInRange($created){
		$beginthisweek = date("Y-m-d",strtotime("-1 week Sunday"));
		$endthisweek = date("Y-m-d",strtotime("this Saturday"));
		if (($created >= $beginthisweek) && ($created <= $endthisweek)){
				return true;
		} else {
				return false;
		}
	}
	/*
	
	*/
	function diffTime($bigTime,$smallTime)
	{
	//input format hh:mm:ss
	
			list($h1,$m1,$s1)=explode(":",$bigTime);
			list($h2,$m2,$s2)=explode(":",$smallTime);
			//echo $bigTime." -> " .$smallTime;
			$second1=$s1+($h1*3600)+($m1*60);//converting it into seconds
			$second2=$s2+($h2*3600)+($m2*60);
			
			
			if ($second1==$second2)
			{
				$resultTime="00:00:00";
				return $resultTime;
				exit();
			}
			
			
			
			if ($second1<$second2) // 
			{
				$second1=$second1+(24*60*60);//adding 24 hours to it.
			}
			
			
			
			$second3=$second1-$second2;
			
			//print $second3;
			if ($second3==0)
			{
				$h3=0;
			}
			else
			{
				$h3=floor($second3/3600);//find total hours
			}
				
			$remSecond=$second3-($h3*3600);//get remaining seconds
			if ($remSecond==0)
			{
				$m3=0;
			}
			else
			{
				$m3=floor($remSecond/60);// for finding remaining  minutes
			}
				
			$s3=$remSecond-(60*$m3);
			
			if($h3==0)//formating result.
			{
				$h3="00";
			}else {if ($h3<=9) {
				$h3zero="0";
				$h3 = "0".$h3;	
			}
			}
			
			if($m3==0)
			{
				$m3="00";
			}else {if ($m3<=9) {
				$m3zero="0";
				$m3 = "0".$m3;	
			}
			}
			if($s3==0)
			{
				$s3="00";
			}else {if ($s3<=9) {
				$s3zero="0";
				$s3 = "0".$s3;	
			}			
			}
	
			$resultTime="$h3:$m3:$s3";
			
			
			return $resultTime;
	
	}	
	/*
	* checkLastPasswordChange
	* USAGE: <?PHP echo checkLastPasswordChange($loggedInUser->user_id)?>
	*/
	function checkLastPasswordChange($userid){
		global $mysqli;
		$sql="SELECT last_password_changed FROM `users` WHERE id=".$userid." AND last_password_changed < DATE_SUB(NOW(), INTERVAL 1 MONTH)";
		$result = $mysqli->query($sql);
		$row = $result->fetch_array();
		if ($result->num_rows) {
			return '<div class="alert alert-warning fade in changepass">
				<button class="close" data-dismiss="alert">
					×
				</button>
				<i class="fa-fw fa fa-danger"></i>
				<strong>Warning</strong> You did not change your password lately. Please <a href="models/changepassword.php" data-toggle="modal" data-target="#remoteModal">click here</a> to change it.
			</div>';
		} else {
			return ;
		}
	}

	/*
	*
	* Remove file if it is exist in database
	*/
	function ifFileIsAvailableRemoveIt($id){
		global $mysqli;
		$sql	= "select img_location from ftree where id=".$id;
		$result = $mysqli->query($sql);
		$upfolder = GE_UPLOAD."upload/userthumbs/";
		$nt = $result->fetch_array();
		if (!empty($nt['img_location']) && $nt['img_location'] !=''){
			$sql1="UPDATE ftree set img_location=NULL WHERE id=".$id;
			$mysqli->query($sql1);
			$loc = $upfolder.$nt['img_location'];
			//echo 'loc:'.$loc;
			if (file_exists($loc)) {
				unlink($loc);
			}
		}			

	}
	//Get File Extension. 
	function getExtension($str) 
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; } 
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	//Compress Image 
	function compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth)
	{
		if($ext=="jpg" || $ext=="jpeg" )
		{
			$src = imagecreatefromjpeg($uploadedfile);
		}
		else if($ext=="png")
		{
			$src = imagecreatefrompng($uploadedfile);
		}
		else if($ext=="gif")
		{
			$src = imagecreatefromgif($uploadedfile);
		}
		else
		{
			$src = imagecreatefrombmp($uploadedfile);
		}
																		
		list($width,$height)=getimagesize($uploadedfile);
		$newheight=($height/$width)*$newwidth;
		$tmp=imagecreatetruecolor($newwidth,$newheight);
		imagealphablending($tmp, false);
		imagesavealpha($tmp, true);
		$trans_layer_overlay = imagecolorallocatealpha($tmp, 220, 220, 220, 127);
		imagefill($tmp, 0, 0, $trans_layer_overlay);
		imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
		$filename = $path.$actual_image_name;
		if($ext=="jpg" || $ext=="jpeg" )
		{
			imagejpeg($tmp,$filename,100);
		}
		else if($ext=="png")
		{
			imagepng($tmp,$filename);
		}
		else 
		{
			imagegif($tmp,$filename);
		}
		imagedestroy($tmp);
		return $filename;
	}
	/*
	*
	*
	*/
	function logActivity($familyid,$userid,$log){
		global $mysqli;
		$mysqli->query("INSERT INTO logactivities set family_id=".$familyid.", user_id=".$userid.", log='".$log."', created='".date("Y-m-d H:i:s")."'");

	}
	/*
	*
	*
	*/
	function isAdmin(){
		global $loggedInUser;
		if ($loggedInUser->perm_id == 10) {
			return true;
		} else {
			return false;
		}
	}

	/*
	*
	*
	*/
	function isSuperAdmin(){
		global $loggedInUser;
		if ($loggedInUser->perm_id == 11) {
			return true;
		} else {
			return false;
		}
	}

	/*
	*
	*
	*/
	function setMessage($message,$messagetype = '') 
	{
		if ($_SESSION["SESSION_MESSAGE"] <> "") 
		{ // Append
			$_SESSION["SESSION_MESSAGE"] .= " -> " . $message;
		} else {
			$_SESSION["SESSION_MESSAGE"] = $message;
		}
		$_SESSION["MESSAGE_TYPE"] = $messagetype;
	}	
	// Show message
	function ShowMessage($slide='yes')
	{	
		$classc='';
		if ($slide == 'yes'){
			$ids = 'id="slidebar"';
		} else {
			$ids = '';
		}
		if (isset($_SESSION["MESSAGE_TYPE"])) {  
			if ($_SESSION["MESSAGE_TYPE"] =='error')  
			{
				$classc ="alert-danger";
				$icon = "fa-times";
			}
			else if ($_SESSION["MESSAGE_TYPE"] =='success')  
			{
				$classc="alert-success";
				$icon = "fa-check";
			}
			else if ($_SESSION["MESSAGE_TYPE"] =='warning')  
			{
				$classc="alert-warning";
				$icon = "fa-warning";
			} else if ($_SESSION["MESSAGE_TYPE"] =='info')  
			{
				$classc="alert-info"; 
				$icon = "fa-info-circle";
			}
			$sMessage = $_SESSION["SESSION_MESSAGE"];
			if ($sMessage <> "") 
			{ 
				echo '<div id="notifybar" class="alert '.$classc.' alert-white rounded">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa '.$icon.'"></i></div>
								'.$_SESSION["MESSAGE_TYPE"].'</strong> '.$sMessage.'
							 </div>';
			}
			$_SESSION["SESSION_MESSAGE"] = ""; // Clear message in Session
			$_SESSION["MESSAGE_TYPE"] = "";
		}
	}
		
	/*
	*
	*
	*/
	function GetNamesFromID($tablename,$selectvalue1,$selectvalue2='',$whereid,$whereidvalue) {
		global $mysqli;
		if (empty($whereidvalue) || $whereidvalue =='') return ;
		$sql="SELECT ";
		if ($selectvalue1 != ''){
			$sql .= $selectvalue1;
		}
		if ($selectvalue2 != ''){
			if ($selectvalue1 != ''){
				$sql .= ", ";
			}
			$sql .= $selectvalue2;
		}
		$sql .=" FROM ".$tablename." WHERE ".$whereid."='".$whereidvalue."'";
		
		$resul22t=$mysqli->query($sql);
		$row = $resul22t->fetch_array();
		$retvalue="";
		if ($selectvalue1 != ''){
			$retvalue .= $row[$selectvalue1];
		}
		if ($selectvalue2 != ''){
			if ($selectvalue1 != ''){
				$retvalue .= " ";
			}
			$retvalue .= $row[$selectvalue2];
		}

		return $retvalue;
	}

	/*
	*
	*
	*/
	function TestArray($po){
		foreach ($po as $key=>$value) {
				echo $key." : ".$value."<BR>";	
		}
	}
	
	/*
	*
	*
	*/
	function TestPost($p){
		if (!empty($p)) {
			echo "<pre>";
			print_r($p);
			echo "</pre>";
		}
	}
	
	/*
	* //usage
	* //ConvertDateFormats("m/d/Y", $date)
	*/
	function ConvertDateFormats($dateformat='', $date='')
	{
		if ($date =='')
			return date($dateformat);
		else			
			return date($dateformat,strtotime($date));
	}
	/*
	*
	*
	*/
	function getSettings($actionname, $selectname,$settingsvalue=''){
			global $mysqli;
			$settingsvalue = htmlspecialchars($settingsvalue,ENT_QUOTES);
			$sql 	 = "SELECT * FROM settings where action_name = '".$actionname."' AND active=1  ORDER BY settings_field  ASC";
			$ret="";
			$ret .= '<select name="'.$selectname.'" id="'.$selectname.'" class="form-control">';
			$results = $mysqli->query($sql);	
			//$ret .= "<option value=''></option>";	
			if ($settingsvalue == ''){
				$ret .= '<option value="">&nbsp;</option>';
			}
			while($nt=$results->fetch_array()){
				if (strcasecmp($nt['settings_field'], $settingsvalue) == 0)
						$sel = ' selected="selected"';	
				else
						$sel ='';
				$ret .= '<option value="'.$nt['settings_field'].'" '.$sel.'>'.$nt['settings_field'].'</option>';
			}
			$ret .= '</select>';
			return $ret;
	}
	
	/*
	*
	*
	*/
	function GetTotalCounts($tablename, $S_search='')
	{
		global $mysqli;
			$sql = "SELECT COUNT(*) as total FROM `$tablename` $S_search";
			$result=$mysqli->query($sql);
			$row = $result->fetch_array();
			if ($result->num_rows > 0) {
				return $row['total'];
			} else {
				return '0';	
			}
	}
//increases the no of views by 1 everytime the page is accessed
// $id is id of page passed in url
	function addviews($id)
	{
		global $mysqli;
		//$id = int_val($_GET['id']); 
		$q = "SELECT * FROM knowledgebase WHERE base_id = {$id} LIMIT 1";
		$sql.="UPDATE knowledgebase SET views=(views+1) WHERE base_id = {$id}";
		$result=$mysqli->query($sql);
		//echo $sql;			
	
	}
	function glossary($id)
	{
		global $mysqli;	
			
		$search=' WHERE 1=1 ';
	$search .= " AND g_term LIKE '".$id."%' ";
	$sql="SELECT * FROM kb_glossary" .$search;
	$results = $mysqli->query($sql);
	//echo $sql;
	$count=0;
	$count = $results->num_rows;
	if($count > 0){
		
		while($row= $results->fetch_array()) {	
	
	$title.= "<strong>".$row["g_term"]."</strong>"."<br/> &nbsp; &nbsp;";
	$des= $row["g_definition"]."<br/> ";
	  $title.=$des;
	
	
	
	
		}
		return $title;
		
      }
	}
	
	function getDatabase($tablename, $column){
		global $mysqli;
		$sql = "SELECT * FROM $tablename ORDER BY $column ASC";
		$results=$mysqli->query($sql);	
		$nt=$results->fetch_array();
			echo $nt['$column'];
			
			
	
		
		
		}
	
	function getCat($id){
			global $mysqli;
			$sql = "SELECT * FROM knowledgebase_cat ORDER BY cat_id ASC";
			echo "<select name=\"cat_id\" id=\"cat_id\" class=\"form-control\">";
			if ($id == '')
				echo "<option value=\"\" selected=\"selected\"></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['cat_id'] == $id)
						$sel = " selected=\"selected\"";	
				else
						$sel ="";
				echo "<option value=\"".$nt['cat_id']."\" $sel>".$nt['category']."</option>";
			}
			echo "</select>";

	}
	function getFAQ($id){
			global $mysqli;
			$sql = "SELECT * FROM knowledgebase_faq ORDER BY faq_id ASC";
			echo "<select name=\"faq_id\" id=\"tags\" class=\"form-control\">";
			if ($id == '')
				echo "<option value=\"\" selected=\"selected\"></option>";
			$results=$mysqli->query($sql);		
			while($nt=$results->fetch_array()){
				if ($nt['faq_id'] == $id)
						$sel = " selected=\"selected\"";	
				else
						$sel ="";
				echo "<option value=\"".$nt['faq_id']."\" $sel>".$nt['tags']."</option>";
			}
			echo "</select>";

	}
	// recursive function to print the items in dropdown select box with only the child items selectable
	/*
	  <label for="sel1">Select Category </label>
            <?PHP 
			       $category=$db->from("snip_category")->fetch();
                   $column = array();
                   $categoryList = fetchCategoryTree($category);
            ?>
            <select class="form-control" id="catname" name="catname" >
                <?php  
			       foreach($categoryList as $cl)
				    {
				       foreach($category as $cat) 
					    {
	                     $column[] = $cat[parent];
                        }
				           if (in_array($cl['id'], $column)) 
				             {
				?>
                               <optgroup label=<?php echo $cl["name"]; ?>></optgroup>
                       <?PHP } else	{		   
			    ?>
                                      <option value="<?php echo $cl["cat_id"] ?>"><?php echo $cl["name"]; ?></option>                        
                            
                               <?php }			                    
					}?>
              
            </select>
			  <?PHP 
			       $category=$db->from("snip_category")->fetch();
				   $id=12;
                   $column = array();
                   $categoryList = fetchfileStr($category,$id);
				   $categoryList=array_reverse($categoryList);
				 //  echo var_dump($categoryList);
            foreach ($categoryList as $cl)
			{
			echo $cl['name']. "/";	
			}
			
			?>
			
*/
//creates an array with id that traces back to the parent from the input id until parent id = 0, ( child id is the input)
	function fetchfileStr($category,$id, $user_tree_array = '') {
 
  if (!is_array($user_tree_array))
    $user_tree_array = array();
		foreach ($category as $cat)
		 {
			 if($cat['cat_id'] == $id){
				 $current_id = $cat['parent'];
			 $user_tree_array[] = array("id" =>$cat['cat_id'], "name" => $cat['cat_name'], "parent" => $cat['parent']);
			 
      $user_tree_array = fetchfileStr($category,$current_id,  $user_tree_array);
		 }
		 }
		 return $user_tree_array;
 }
 
 function fetchchildStr($category,$id, $user_tree_array = '') {
 
  if (!is_array($user_tree_array))
    $user_tree_array = array();
		foreach ($category as $cat)
		 {
			 if(($cat['parent'] == 0)){
			 $parent = $cat['cat_id'];	
			
			 $user_tree_array[] = array("id" =>$cat['cat_id'], "name" => $cat['cat_name'], "parent" => $cat['parent']);
			 
			  
			 
      $user_tree_array = fetchchildStr($category,$parent,  $user_tree_array);
		 }
		 }
		 return $user_tree_array;
 }
 
 
 

function fetchCategoryTree($category,$parent = 0, $spacing = '', $user_tree_array = '') { //createa an array tree structure for parent and childeren where parents's parent id is 0
 
  if (!is_array($user_tree_array))
    $user_tree_array = array();
		foreach ($category as $cat)
		 {
			 if($cat['parent'] == $parent){
				 $current_id = $cat['cat_id'];
			 $user_tree_array[] = array("id" =>$cat['cat_id'], "name" => $spacing . $cat['cat_name'], "parent" => $cat['parent']);
			 
      $user_tree_array = fetchCategoryTree($category,$current_id, $spacing . '&nbsp;&nbsp;', $user_tree_array);
		 }
		 }
		 return $user_tree_array;
 }
 
 
 function checkParent($category, $id)
	{
		foreach ($category as $cat)
		{
		 	if(($cat['cat_id'] == $id) )
				
					{
						
						foreach ($category as $cat1)
							{			
								 if(($cat1['cat_id'] == $cat['parent']) )
								 	
								       if( $cat1['parent']==0)
									   return $cat['cat_id'];
									   
									   
				 
			 				       
								
							}
				                
		 			  }
					
			 }
			   
	 }
 
  function childId($category, $id, $list){ // lists id of all the  childrens
	  //$list= array();
   foreach ($category as $row)
		   {
				if($row['parent']==$id)
				{
				$temp=$row['cat_id'];
		$list[]=array("id"=>$temp);
		$list=childId($category,$temp,$list);
				
				
				
		
			}
		   }
		   return $list;
  }//echo $row['content'];
		 
