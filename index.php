<?PHP 
require_once("header.php");


define('IS_IN_MODULE', true);
$module = !empty($_GET['module']) ? $_GET['module'] : 'dashboard';
$submodule = !empty($_GET['submodule']) ? $_GET['submodule'] : '';
if(isset($module))
{
	if ($submodule =="") {	
		if(file_exists($module_base_dir.$module. '/'.$module.$module_base_file))
		{
			include($module_base_dir.$module. '/'.$module.$module_base_file); // Include our module
		} else {
			//echo "Module Not Found!!. Please try again later...".$module_base_dir.$module. '/'.$module.$module_base_file;
			header("Location: index.php?module=errors&submodule=module_not_found");
		}
	} else {
		if(file_exists($module_base_dir.$module. '/'.$submodule.$module_base_file))
		{
				include($module_base_dir.$module. '/'.$submodule.$module_base_file); // Include our module
		}
		else {
				//echo "SubModule Not Found!!. Please try again later...".$module_base_dir.$module. '/'.$submodule.$module_base_file;
				header("Location: index.php?module=errors&submodule=module_not_found");
		}
	}
}
 else
{
?>
		<!--  start content-table-inner ...................................................................... START -->
        <br />
<br />
			
<?PHP	
//header("Location: index.php?module=Errors&submodule=access_denied");
header("Location: login.php");
} //end of module
require_once("footer.php")
