<?PHP
	ob_start();
 	require_once("models/config.php");
	//if(!isUserLoggedIn()) { header("Location: index.php?module=dashboard"); die(); }
	header("Expires: Mon, 26 Jul 2010 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
	header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Content-Type: text/html; charset=utf-8");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title><?PHP echo $websiteName?></title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS 
<link href="css/bootstrap.css" rel="stylesheet">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="fonts/font-awesome4.1.0.css">
<link href="css/nanoscroller.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
</head>
<body class="animated">
    <div class="login-container animated fadeInDown" style="width:250px">
        <div class="loginbox bg-white">
            <div class="loginbox-title">Ideal Desk<BR>SIGN IN</div>
            <div class="loginbox-social"><br><br>
            <span id="showerror"></span>
<br>
</div>
<form id="login-form" class="smart-form client-form" >
            <div class="loginbox-textbox">
              <input type="text" class="form-control" name="username" id="username" placeholder="username" />
            </div>
            <div class="loginbox-textbox">
                <input type="password" class="form-control" name="password123" id="password123" placeholder="Password" />
            </div>
            <div class="loginbox-forgot">
            </div>
            <div class="loginbox-submit">
              <input type="submit" class="btn btn-primary btn-block" value="Login" id="submitbtn">
            </div>
</form>            
        </div>
    </div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> 
<script type="text/javascript" src="js/jPushMenu.js"></script> 
<script type="text/javascript" src="js/nanoscroller.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 

<!-- Bootstrap core JavaScript
================================================== 
<script src="js/bootstrap.min.js"></script>--> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript">
			$(function() {
			 	$("#login-form").submit(function(e){
					e.preventDefault();
					$("#submitbtn").val('please wait');
					$("#showerror").html('');
					if ($("#password123").val() != '' && $("#username").val() != ''){
						$("#submitbtn").html('signing');
						$.ajax({
							type:"POST",
							url: "ajax_login.php",
							data: $(this).serialize(),
							success: function(html){
								html=html.trim();
								if (!$.isEmptyObject(html) && html == 1 && html.length != 0 && html !='' && html !== null) {
									window.location=('index.php?module=dashboard');
								} else {
									$("#submitbtn").html('Sign');
									$("#password123").val('');
									$("#username").val('');
									$("#showerror").html(html);
									$("#submitbtn").val('Login');
								}
							}
						});
					} else {
						$("#showerror").html('Username or Password is required');
						$("#submitbtn").val('Login');
					}
			 	});
			});
</script>				    
</body>
<!--Body Ends-->
</html>
