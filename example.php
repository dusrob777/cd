<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title></title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS 
<link href="css/bootstrap.css" rel="stylesheet">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="fonts/font-awesome4.1.0.css">
<link href="css/nanoscroller.css" rel="stylesheet" />
<link href="css/datetimepicker.css" rel="stylesheet" />
<link href="css/summernote.css" rel="stylesheet" />
<link href="css/summernote-bs3.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/icheckskins/flat/blue.css" rel="stylesheet">
<link href="css/helpdesk.css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() 
        { 
            $('#notifybar').slideUp(); 
        }, 5000); 
    });
</script>
</head>
<body class="animated">
<div id="cl-wrapper">
  <div class="container-fluid" id="pcont">
    <div class="cl-mcont"> 
      <!---->
      <div class="idpanel idbox">
        <div class="row"> 
          <!---->
          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="panelbody">
              <div class="content">
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-primary" style="width: 20%">20%</div>
                </div>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-success" style="width: 40%">40%</div>
                </div>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-info" style="width: 60%">60%</div>
                </div>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-warning" style="width: 80%">80%</div>
                </div>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-danger" style="width: 100%">100%</div>
                </div>
              </div>
            </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="panelbody">
              <div class="content">
                <div class="progress progress-striped">
                  <div class="progress-bar progress-bar-primary" style="width: 20%">20%</div>
                </div>
                <div class="progress progress-striped">
                  <div class="progress-bar progress-bar-success" style="width: 40%">40%</div>
                </div>
                <div class="progress progress-striped">
                  <div class="progress-bar progress-bar-info" style="width: 60%">60%</div>
                </div>
                <div class="progress progress-striped">
                  <div class="progress-bar progress-bar-warning" style="width: 80%">80%</div>
                </div>
                <div class="progress progress-striped">
                  <div class="progress-bar progress-bar-danger" style="width: 100%">100%</div>
                </div>
              </div>
            </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="panelbody">
              <div>
                <div class="header">
                  <div class="pull-right actions"> </div>
                  <h3>Border Color</h3>
                </div>
                <div class="content">
                  <div class="alert alert-primary alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Great!</strong> Can you say supercalifragilisticoexpialidoso? </div>
                  <div class="alert alert-success alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Changes has been saved successfully! </div>
                  <div class="alert alert-info alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="icon"><i class="fa fa-info-circle"></i></div>
                    <strong>Info!</strong> You have 3 new messages in your inbox. </div>
                  <div class="alert alert-warning alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="icon"><i class="fa fa-warning"></i></div>
                    <strong>Alert!</strong> Don't forget to save your data. </div>
                  <div class="alert alert-danger alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> The server is not responding, try again later. </div>
                </div>
              </div>
            </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="panelbody"><div>
						<div class="header">
							<div class="pull-right actions">
							</div>							
							<h3>Theme Alerts</h3>
						</div>
						<div class="content">
							 
							 <div class="alert alert-primary alert-white-alt2 rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-check"></i></div>
								<strong>Great!</strong> Can you say supercalifragilisticoexpialidoso?
							 </div>
							 <div class="alert alert-success alert-white-alt2 rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-check"></i></div>
								<strong>Success!</strong> Changes has been saved successfully!
							 </div>
							 
							 <div class="alert alert-info alert-white-alt2 rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-info-circle"></i></div>
								<strong>Info!</strong> You have 3 new messages in your inbox.
							 </div>
							 
							 <div class="alert alert-warning alert-white-alt2 rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-warning"></i></div>
								<strong>Alert!</strong> Don't forget to save your data.
							 </div>
							 
							 <div class="alert alert-danger alert-white-alt2 rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-times-circle"></i></div>
								<strong>Error!</strong> The server is not responding, try again later.
							 </div>
							 
						</div>
					</div> </div>
          </div>
          <!----> 
        </div>
        <!--// row-->
        
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail clean tile-green">
              <div class="content">
                <h1 class="text-left">170</h1>
                <p>New Visitors</p>
              </div>
              <div class="icon"><i class="fa fa-flag"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail clean tile-purple">
              <div class="content">
                <h1 class="text-left">72</h1>
                <p>New Views</p>
              </div>
              <div class="icon"><i class="fa fa-eye"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail clean tile-concrete">
              <div class="content">
                <h1 class="text-left">35</h1>
                <p>New Downloads</p>
              </div>
              <div class="icon"><i class="fa fa-download"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail clean tile-blue">
              <div class="content">
                <h1 class="text-left">5</h1>
                <p>Responses</p>
              </div>
              <div class="icon"><i class="fa fa-reply"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!----> 
        </div>
        <!--// row-->
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail tile-green">
              <div class="content">
                <h1 class="text-left">170</h1>
                <p>New Visitors</p>
              </div>
              <div class="icon"><i class="fa fa-flag"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!---->
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="fd-tile detail tile-red">
              <div class="content">
                <h1 class="text-left">24</h1>
                <p>New Comments</p>
              </div>
              <div class="icon"><i class="fa fa-comments"></i></div>
              <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a> </div>
          </div>
          <!----> 
        </div>
        <!--// row-->
        <div class="row">
          <div class="col-md-3 col-sm-6"> </div>
        </div>
        <!--// row--> 
        
      </div>
      <!----> 
      
    </div>
  </div>
</div>
<script type="text/javascript" src="js/jPushMenu.js"></script> 
<script type="text/javascript" src="js/nanoscroller.js"></script> 
<script src="js/moment.js"></script> 
<script src="js/daterangepicker.js"></script> 
<script src="js/bootbox.js"></script> 
<script src="js/summernote.min.js"></script> 
<script src="js/toaster.js"></script> 
<script src="js/icheck.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 

<!-- Bootstrap core JavaScript
================================================== 
<script src="js/bootstrap.min.js"></script>--> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
