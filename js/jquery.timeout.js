'use strict';

(function( $ ){
    jQuery.sessionTimeout = function( options ) {
        var defaults = {
            message             : 'Your session is about to expire.',
            keepAliveUrl        : 'models/keepalive.php',
            ajaxData            : '',
            redirUrl            : 'logout.php',
            logoutUrl           : 'logout.php',
            warnAfter           : 900000,   // 15 minutes
            redirAfter          : 1500000,  // 20 minutes
            keepAliveInterval   : 60000,
            keepAlive           : true,
            ignoreUserActivity  : false,
            onWarn              : false,
            onRedir             : false
        };

        var opt = defaults,
            timer, timers, countdown;
		var sec = 180;
		
        // extend user-set options over defaults
        if ( options ) {
            opt = $.extend( defaults, options );
        }

        // some error handling if options are miss-configured
        if(opt.warnAfter >= opt.redirAfter){
            // for IE8 and earlier
            if (typeof console !== "undefined" || typeof console.error !== "undefined") {
                console.error('Bootstrap-session-timeout plugin is miss-configured. Option "redirAfter" must be equal or greater than "warnAfter".');
            }
            return false;
        }

        // unless user set his own callback function, prepare bootstrap modal elements and events
        if(typeof opt.onWarn !== 'function'){
            // create timeout warning dialog
            $('body').append('<div class="modal fade" id="sessionTimeout-dialog"><div class="modal-dialog" style="width:340px"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h5 class="modal-title">Your Session is About to Expire!</h5></div><div class="modal-body" style="text-align:center">'+ opt.message + ' <span id="spantimer">240</span> secs</div><div class="modal-footer"><button id="sessionTimeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Stay Connected</button></div></div></div></div>');

            // "Logout" button click
            $('#sessionTimeout-dialog-logout').on('click', function () { window.location = opt.logoutUrl; });
            // "Stay Connected" button click
            $('#sessionTimeout-dialog').on('hide.bs.modal', function () {
                // restart session timer
                startSessionTimer();
				clearInterval(timers);//count down
            });
        }

        // reset timer on any of these events
        if (!opt.ignoreUserActivity) {
            $(document).on('keyup mouseup mousemove touchend touchmove', function() {
                startSessionTimer();
				
            });
        }

        // keeps the server side connection live, by pingin url set in keepAliveUrl option
        // keepAlivePinged is a helper var to ensure the functionality of the keepAliveInterval option
        var keepAlivePinged = false;
        function keepAlive () {
            if (!keepAlivePinged){
                $.ajax({
                    type: 'POST',
                    url: opt.keepAliveUrl,
                    data: opt.ajaxData
                });
                keepAlivePinged = true;
                setTimeout(function() {
                    keepAlivePinged = false;
                }, opt.keepAliveInterval);
            }
        }

        function startSessionTimer(){
            // console.log('startSessionTimer()');
            // clear session timer
            clearTimeout(timer);
			
            // if keepAlive option is set to "true", ping the "keepAliveUrl" url
            if (opt.keepAlive) {
                keepAlive();
            }

            // set session timer 
            timer = setTimeout(function(){
                // check for onWarn callback function and if there is none, launch dialog
                if(typeof opt.onWarn !== 'function'){
                    $('#sessionTimeout-dialog').modal('show');
						startCountDown(); //count down
					
                }
                else {
                    opt.onWarn('warn');
                }
                // start dialog timer
                startDialogTimer();
            }, opt.warnAfter);
        }

        function startDialogTimer(){
            // console.log('startDialogTimer()');
            // clear session timer
            clearTimeout(timer);
			
            // set dialog timer 
            timer = setTimeout(function(){
                // check for onRedir callback function and if there is none, launch redirect
                if(typeof opt.onRedir !== 'function'){
                    startDialogTimer('start');
                    window.location = opt.redirUrl;
                }
                else {
                    opt.onRedir();
                }
            }, (opt.redirAfter - opt.warnAfter));
        }
		//hakan
		function startCountDown(){
			sec=180;
			timers = setInterval(function() { 
				$('#spantimer').text(--sec);
				//console.log(sec);
				if (sec == 0) {
				  window.location = opt.redirUrl;
				} 
			}, 1000);
		}
        // start session timer
        startSessionTimer();
    };
})( jQuery );
$.sessionTimeout({
    message: '',
    keepAliveUrl: 'models/keepalive.php',
    logoutUrl: 'logout.php',//'index.php?module=dashboard',
    redirUrl: 'logout.php',
    warnAfter: 900000,
    redirAfter: 1500000
});