<?PHP
	ob_start();
 	require_once("models/config.php");
	//if(!isUserLoggedIn()) { header("Location: index.php?module=dashboard"); die(); }
	header("Expires: Mon, 26 Jul 2010 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
	header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Content-Type: text/html; charset=utf-8");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0

?>

<?PHP

	
	if(isset($data->success) AND $data->success=true){
	
	if( !isUserLoggedIn() ){
	
	}
		


} 



?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Sign In - Ideal Desk</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/login.css" rel="stylesheet" type="text/css">
    <!-- google recaptcha-->
  
    
</head>
<body class="page-signin">
	<div id="page-signin-bg">
		<!-- Background overlay -->
		<div class="overlay"></div>
		<!-- Replace this with your bg image -->
	</div>
	<!-- / Page background -->

	<!-- Container -->
	
	<span class="FormElement">Message Sent</span>
	<div class="signin-container">
    
		<!-- Left side -->
		<div class="signin-info">
			<a href="index.html" class="logo">
				codeDirectory
			</a> <!-- / .logo -->
			<div class="slogan">
				Company code Repository
			</div> <!-- / .slogan -->
			<ul>
				<li><i class="fa fa-sitemap signin-icon"></i> Restricted site.</li>
				<li><i class="fa fa-file-text-o signin-icon"></i> This site is for Idyllic users only</li>
				<li><i class="fa fa-outdent signin-icon"></i> If you come to this site by mistake, please navigate away</li>
				<li><i class="fa fa-heart signin-icon"></i> Recording in process.</li>
			</ul> <!-- / Info list -->
		</div>
		<!-- / Left side -->
		<!-- Right side -->
		<div class="signin-form">
			<!-- Form -->
			<form id="login-form">
				<div class="signin-text">
					<span>Sign In to your account</span>
				</div> 
              
                
                <div class="FormElement"></div>
              
             
 

			  <div class="form-group">
					<input type="text" name="username" id="username" class="form-control input-sm" placeholder="Username or email">
					<span class="fa fa-user"></span>
				</div> <!-- / Username -->

				<div class="form-group">
					<input type="password" name="password123" id="password123" class="form-control input-sm" placeholder="Password">
					<span class="fa fa-lock signin-form-icon"></span>
				</div> <!-- / Password -->

				<div class="form-actions">
					<input type="submit" value="SIGN IN" class="btn btn-info btn-block" id="submitbtn">
                    
                  
					
				</div>
                 <!-- / .form-actions -->
			</form>
            <div id="showerror"></div>
			<!-- / Form -->
			<div class="signin-with">
				Welcome to Idyllic Enterprises
			</div>
		</div>
		<!-- Right side -->
	</div>
	<!-- / Container -->

	<div class="not-a-member">
		Copyright&copy; 2014-<?PHP echo date("Y")?> Designed and maintained by Idyllic Enterprises
	</div>

	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>

<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
			$(function() {
			 	$("#login-form").submit(function(e){
					e.preventDefault();
					$("#submitbtn").val('please wait');
					$("#showerror").html('');
					if ($("#password123").val() != '' && $("#username").val() != ''){
						$("#submitbtn").html('signing');
						$.ajax({
							type:"POST",
							url: "ajax_login.php",
							data: $(this).serialize(),
							success: function(html){
								html=html.trim();
								if (!$.isEmptyObject(html) && html == 1 && html.length != 0 && html !='' && html !== null) {
									window.location=('index.php?module=codedirectory&submodule=home');
								} else {
									$("#submitbtn").html('Sign');
									$("#password123").val('');
									$("#username").val('');
									$("#showerror").html(html);
									$("#submitbtn").val('Login');
								}
							}
						});
					} else {
						$("#showerror").html('Username or Password is required');
						$("#submitbtn").val('Login');
					}
			 	});
			});
</script>	
</body>
</html>
